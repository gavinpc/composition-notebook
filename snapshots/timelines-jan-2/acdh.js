// the tangle of acdh.html, from
// Array.from(document.querySelectorAll('textarea'), ta => ta.value).join('\n')
// (then replace \" with ")
// AAAAND replace \\ with \ in regexp's...
// (all that just because of how the string is printed in the console)
// and add the module exports
// and then remove stuff referencing `document`
// OH and I had to implement acdh_parse for millennium
// OH and I had to implement acdh_narrower for millennium
const ACDHUT = "https://vocabs.acdh.oeaw.ac.at/unit_of_time/";
const ACDHDATE = "https://vocabs.acdh.oeaw.ac.at/date/";

const acdhut = Object.fromEntries(
  "Millennium Century Decade Year Month Day"
    .split(" ")
    .map(name => [name, `${ACDHUT}${name}`])
);

const int = s => parseInt(s, 10);

const YYYYMMDD = /(-?\\d\\d\\d\\d)-(\\d\\d)(?:-(\\d\\d))?/;

/* THIS is not used
const ordinal_en = (n) =>
  `${n}${
    Math.abs(n) % 100 === 11
      ? "th"
      : ORDINAL_SUFFIXES_EN[Math.abs(n) % 10] ?? "th"
  }`;
*/

const pad = len => n =>
  (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");

const pad2 = pad(2);
const pad3 = pad(3);
const pad4 = pad(4);

const MILLENNIUM = /^((-?)\d{4,5})\/(\2\d0000?)$/;
const DATE = /^(-?)(\d\d)(\d)?(\d)?(?:-(\d\d))?(?:-(\d\d))?$/;

function acdh_parse(iso) {
  if (typeof iso !== "string") return;

  // GPC ADDED ===================================
  // represent millennium by the ending year
  const mill = iso.match(MILLENNIUM);
  if (mill) {
    const [, from, bc, to] = mill;
    const n = parseInt(to, 10);
    if (n !== parseInt(from, 10) + 999)
      throw new Error(`Ill-formed millennium (999-wise) ${iso}`);
    return { input: iso, type: acdhut.Millennium, bc, millennium: n };
  }
  // ==================================

  const match = iso.match(DATE);
  if (!match) return;

  const [, bc, cc, d, y, mm, dd] = match;
  return {
    input: iso,
    type: dd
      ? acdhut.Day
      : mm
      ? acdhut.Month
      : y
      ? acdhut.Year
      : d
      ? acdhut.Decade
      : acdhut.Century,
    bc,
    mm,
    dd,
    century: cc,
    ...(cc && d && { decade: `${bc}${cc}${d}` }),
    ...(cc && d && y && { year: `${bc}${cc}${d}${y}` }),
    ...(cc && d && y && mm && { month: `${bc}${cc}${d}${y}-${mm}` }),
    ...(cc && d && y && mm && dd && { day: `${bc}${cc}${d}${y}-${mm}-${dd}` }),
  };
}

// accepts: string
// returns: string | undefined
// bind: { parse: acdh parse }
// provides: acdh_broader
//
function acdh_broader(iso) {
  const parsed = acdh_parse(iso);
  if (parsed) {
    const { type } = parsed;
    if (type === acdhut.Decade) {
      // Century is off-by-one from that of their “narrower” decades.
      // i.e. Century 20 contains decades 190, 191... etc.
      return pad2((parsed.bc ? -1 : 1) * (parseInt(parsed.century, 10) + 1));
    }
    if (type === acdhut.Year) return parsed.decade;
    if (type === acdhut.Month) return parsed.year;
    if (type === acdhut.Day) return parsed.month;
  }
}

function acdh_narrower(iso) {
  const parsed = acdh_parse(iso);
  if (!parsed) return undefined;

  // GPC ADDED ===========================
  // Millennium => Century*
  if (parsed.type === acdhut.Millennium) {
    const n = int(parsed.millennium) / 1000;
    if (n !== Math.floor(n)) throw new Error(`BAD MILLENNIUM ${iso}`);
    return Array.from(Array(10).keys(), i =>
      pad2((n - 1) * 10 + i + (parsed.bc ? 0 : 1))
    );
  }
  // =======================================

  // Century => Decade*
  if (parsed.type === acdhut.Century) {
    // Deal with centuries being off-by-one
    const n = int(parsed.century);
    const nn = parsed.bc ? n + 1 : n - 1;
    const base = parsed.bc + pad2(Math.abs(nn));
    return Array.from(Array(10).keys(), i => `${base}${i}`);
  }

  // Decade => Year*
  if (parsed.type === acdhut.Decade) {
    return Array.from(Array(10).keys(), i => `${parsed.input}${i}`);
  }

  // Year => Month*
  if (parsed.type === acdhut.Year) {
    return Array.from(Array(12).keys(), i => `${parsed.input}-${pad2(i + 1)}`);
  }

  // Month => Day*
  if (parsed.type === acdhut.Month) {
    const ret = [];
    // Needs to be local time else you end up in the previous day
    const date = new Date(int(parsed.year), int(parsed.mm) - 1);
    const month = date.getMonth();
    while (month === date.getMonth()) {
      const day = date.getDate();
      ret.push(`${parsed.month}-${pad2(day)}`);
      date.setDate(day + 1);
    }
    return ret;
  }
}

module.exports = { ACDHUT, acdhut, acdh_parse, acdh_narrower };
