(() => {
  function make_system() {
    let _lifeline;
    const _sequences = new Map();
    const _completed = new Set();
    const performance_now = performance.now.bind(performance);

    function step(budget = 10, now = performance_now) {
      _lifeline = null;
      if (_sequences.size === 0) return;
      const [[thing, { iterator }]] = _sequences;
      let tally = 0;
      let begin = now();
      const stop_by = begin + budget;
      while (begin < stop_by) {
        const result = iterator.next();
        const end = now();
        ++tally;
        if (result.done) {
          _sequences.delete(thing);
          _completed.add(thing);
          if (_sequences.size === 0) return;
          break;
        }
        const cost = end - begin;
        if (cost > budget) {
          console.warn(
            `Single step took ${cost}ms, more than budget of ${budget}`
          );
          break;
        }
        begin = end;
      }

      setTimeout(play, 10);
    }
    function play() {
      _lifeline ??= requestAnimationFrame(step);
    }
    function pause() {
      if (_lifeline) {
        cancelAnimationFrame(_lifeline);
        _lifeline = null;
      }
    }
    function add_sequence(thing) {
      if (_sequences.has(thing) || _completed.has(thing)) return;
      if (typeof thing.step !== "function")
        throw new Error(`System expected something with a ‘step’ function.`);
      if (thing.step.length !== 0)
        throw new Error(
          `System expected step function to accept no arguments.`
        );
      const iterator = thing.step();
      if (typeof iterator.next !== "function")
        throw new Error(`System expected step function to return an iterable.`);

      _sequences.set(thing, { thing, iterator });
      play();
    }
    return {
      play,
      pause,
      get playing() {
        return !!_lifeline;
      },
      add_sequence,
      _completed,
      _sequences,
    };
  }

  // see acdh.js for the s
  const acdh = (() => {
    const ACDHUT = "https://vocabs.acdh.oeaw.ac.at/unit_of_time/";
    const ACDHDATE = "https://vocabs.acdh.oeaw.ac.at/date/";

    const acdhut = Object.fromEntries(
      "Millennium Century Decade Year Month Day"
        .split(" ")
        .map(name => [name, `${ACDHUT}${name}`])
    );

    const int = s => parseInt(s, 10);

    const YYYYMMDD = /(-?\\d\\d\\d\\d)-(\\d\\d)(?:-(\\d\\d))?/;

    const pad = len => n =>
      (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");

    const pad2 = pad(2);
    const pad3 = pad(3);
    const pad4 = pad(4);

    const MILLENNIUM = /^((-?)\d{4,5})\/(\2\d0000?)$/;
    const DATE = /^(-?)(\d\d)(\d)?(\d)?(?:-(\d\d))?(?:-(\d\d))?$/;

    function acdh_parse(iso) {
      if (typeof iso !== "string") return;

      // e.g. 2nd millennium is https://vocabs.acdh.oeaw.ac.at/date/1001/2000
      const mill = iso.match(MILLENNIUM);
      if (mill) {
        const [, from, bc, to] = mill;
        const n = parseInt(to, 10);
        if (n !== parseInt(from, 10) + 999)
          throw new Error(`Ill-formed millennium (999-wise) ${iso}`);
        return { input: iso, type: acdhut.Millennium, bc, millennium: n };
      }

      const match = iso.match(DATE);
      if (!match) return;

      const [, bc, cc, d, y, mm, dd] = match;
      return {
        input: iso,
        type: dd
          ? acdhut.Day
          : mm
          ? acdhut.Month
          : y
          ? acdhut.Year
          : d
          ? acdhut.Decade
          : acdhut.Century,
        bc,
        mm,
        dd,
        century: cc,
        ...(cc && d && { decade: `${bc}${cc}${d}` }),
        ...(cc && d && y && { year: `${bc}${cc}${d}${y}` }),
        ...(cc && d && y && mm && { month: `${bc}${cc}${d}${y}-${mm}` }),
        ...(cc &&
          d &&
          y &&
          mm &&
          dd && { day: `${bc}${cc}${d}${y}-${mm}-${dd}` }),
      };
    }

    function acdh_broader(iso) {
      const parsed = acdh_parse(iso);
      if (parsed) {
        const { type } = parsed;
        if (type === acdhut.Decade) {
          // Century is off-by-one from that of their “narrower” decades.
          // i.e. Century 20 contains decades 190, 191... etc.
          return pad2(
            (parsed.bc ? -1 : 1) * (parseInt(parsed.century, 10) + 1)
          );
        }
        if (type === acdhut.Year) return parsed.decade;
        if (type === acdhut.Month) return parsed.year;
        if (type === acdhut.Day) return parsed.month;
      }
    }

    function acdh_narrower(iso) {
      const parsed = acdh_parse(iso);
      if (!parsed) return undefined;

      // Millennium => Century*
      if (parsed.type === acdhut.Millennium) {
        const n = int(parsed.millennium) / 1000;
        if (n !== Math.floor(n)) throw new Error(`BAD MILLENNIUM ${iso}`);
        return Array.from(Array(10).keys(), i =>
          pad2((n - 1) * 10 + i + (parsed.bc ? 0 : 1))
        );
      }

      // Century => Decade*
      if (parsed.type === acdhut.Century) {
        // Deal with centuries being off-by-one
        const n = int(parsed.century);
        const nn = parsed.bc ? n + 1 : n - 1;
        const base = parsed.bc + pad2(Math.abs(nn));
        return Array.from(Array(10).keys(), i => `${base}${i}`);
      }

      // Decade => Year*
      if (parsed.type === acdhut.Decade) {
        return Array.from(Array(10).keys(), i => `${parsed.input}${i}`);
      }

      // Year => Month*
      if (parsed.type === acdhut.Year) {
        return Array.from(
          Array(12).keys(),
          i => `${parsed.input}-${pad2(i + 1)}`
        );
      }

      // Month => Day*
      if (parsed.type === acdhut.Month) {
        const ret = [];
        // Needs to be local time else you end up in the previous day
        const date = new Date(int(parsed.year), int(parsed.mm) - 1);
        const month = date.getMonth();
        while (month === date.getMonth()) {
          const day = date.getDate();
          ret.push(`${parsed.month}-${pad2(day)}`);
          date.setDate(day + 1);
        }
        return ret;
      }
    }

    const next = {
      [acdhut.Century]: _ =>
        _.input === "-01" ? "01" : pad2(int(_.century) + 1),
      [acdhut.Decade]: _ => pad3(int(_.decade) + 1),
      [acdhut.Year]: _ => pad4(int(_.year) + 1),
      [acdhut.Month]: _ => {
        if (_.mm === "12") return iso_next(_.year) + "-01";
        return _.bc + _.year + "-" + pad2(int(_.mm) + 1);
      },
      [acdhut.Day]: _ => {
        const timestamp = Date.parse(_.day);
        if (isNaN(timestamp)) return;
        const date = new Date(timestamp);
        date.setDate(date.getDate() + 1);
        return _.bc + date.toISOString().slice(0, 10);
      },
    };

    const prev = {
      [acdhut.Century]: _ =>
        _.input === "01" ? "-01" : pad2(int(_.century) - 1),
      [acdhut.Decade]: _ => pad3(int(_.decade) - 1),
      [acdhut.Year]: _ => pad4(int(_.year) - 1), // maybe a boundary case here
      [acdhut.Month]: _ => {
        if (_.mm === "01") return iso_previous(_.year) + "-12";
        return _.bc + _.year + "-" + pad2(int(_.mm) - 1);
      },
      [acdhut.Day]: _ => {
        const timestamp = Date.parse(_.day);
        if (isNaN(timestamp)) return;
        const date = new Date(timestamp);
        date.setDate(date.getDate() - 1);
        return _.bc + date.toISOString().slice(0, 10);
      },
    };

    // analog of time:meets
    const iso_next = date => {
      const parsed = acdh_parse(date);
      if (parsed) return next[parsed.type](parsed);
    };
    // analog of time:isMetBy
    const iso_previous = date => {
      const parsed = acdh_parse(date);
      if (parsed) return prev[parsed.type](parsed);
    };

    const acdh_type = iso => acdh_parse(iso)?.type;

    return {
      acdhut,
      acdh_type,
      iso_next,
      iso_previous,
      acdh_broader,
      acdh_narrower,
    };
  })();
  globalThis.acdh = acdh;

  // https://github.com/tc39/proposal-regex-escaping/blob/main/polyfill.js
  // seeAlso https://github.com/tc39/proposal-regex-escaping/issues/37
  // This is safe for whole patterns.  The objections are to certain joins.
  const RegExp_escape = s => String(s).replace(/[\\^$*+?.()|[\]{}]/g, "\\$&");

  const decode_wiki_title = s => decodeURIComponent(s.replace(/_/g, " "));

  const graph = new Map();
  Object.defineProperty(globalThis, "graph", { value: graph }); // for console

  function preprocess_item(meta, data) {
    if (!("meta" in data)) data.meta = meta;
    if (!("text" in data)) {
      // DOMParser creates a whole HtmlDocument.
      const ele = document.createElement("div");
      ele.innerHTML = data.html;

      // BUT this includes `items`, which makes no sense
      const note = { ...meta, ...data };
      // Build graph nodes from references.  It's completely unrelated to
      // setting the record's text, but since we have an element to query...
      for (const wiki_link of ele.querySelectorAll(`a[wiki]`)) {
        const wiki = wiki_link.getAttribute("wiki");
        const label = decode_wiki_title(wiki);
        const key = label.toLocaleLowerCase();
        let node = graph.get(key);
        if (node) {
          node.wiki = wiki;
          node.label = label;
          node.referencedBy.push(note);
          node.hits++;
        } else {
          graph.set(
            key,
            (node = { wiki, label, referencedBy: [note], hits: 1 })
          );
        }
      }

      data.text = ele.innerText;
    }
  }

  function record_for(year) {
    const time = year.toString();
    return kb.data.find(record => record.aboutTime === time);
  }

  function is_period_wiki(wiki) {
    const it = wiki.trim().toLowerCase();
    return kb.data.find(record => record.wiki.toLowerCase() === it);
  }

  function* links_from(year) {
    for (const item of record_for(year)?.items || []) {
      const ele = document.createElement("div");
      ele.innerHTML = item.html;
      for (const wiki_link of ele.querySelectorAll(`a[wiki]`)) {
        const wiki = wiki_link.getAttribute("wiki");
        // TODO: also filter out months & days of year
        if (!is_period_wiki(wiki)) yield wiki;
      }
    }
  }

  function links_from_map(year) {
    const map = new Map();
    for (const wiki of links_from(year))
      if (map.has(wiki)) map.set(wiki, map.get(wiki) + 1);
      else map.set(wiki, 1);
    return map;
  }

  function preprocess_items() {
    function* step() {
      for (const record of kb.data)
        for (const item of record.items) {
          preprocess_item(record, item);
          yield;
        }
    }
    return { step };
  }

  function wiki_url_for(name, opts) {
    const lang = opts?.lang ?? "en";
    const m = opts?.mobile ? "m." : "";
    return `https://${lang}.${m}wikipedia.org/wiki/${name}`;
  }

  function render_wiki_quote(wiki, markup) {
    // prettier-ignore
    return `<blockquote cite="${wiki_url_for(wiki, { lang: "en" })}">${markup}</blockquote>`;
  }

  function kb_text_search_process(needle, doc = document) {
    const output = doc.createElement("output");
    output.dataset.query = needle;

    function* step() {
      let match_count = 0;
      const pattern = RegExp_escape(needle);
      const flags = needle === needle.toLocaleLowerCase() ? "i" : "";
      const regex = new RegExp(pattern, `g${flags}`);
      const p = (record, item) => {
        preprocess_item(record, item);
        return regex.test(item.text);
      };
      // map pin markers https://stackoverflow.com/a/54331400

      for (const record of kb.data)
        if (record.items)
          for (const data of record.items) {
            if (p(record, data)) {
              if (++match_count >= 8) break; // else we blow up.  but who should do this?
              // this skirts 2 tricky problems:
              // - match the text, not the markup
              // - wrap the the text (not the markup) spanning/balancing elements
              // See https://github.com/julmot/mark.js
              const markup = data.html.replace(regex, "<mark>$&</mark>");
              // prettier-ignore
              output.innerHTML += `<article>
                <header aria-role="contentinfo">
                  <p>
                    This item from
                    <cite>
                      an article about <a wiki="${record.wiki}"><time>${record.label}</time></a>
                    </cite>
                    matches “${needle}”
                  </p>
                </header>
                ${render_wiki_quote(record.wiki, markup)}
              </article>`;
            }
            // more overhead to yield here vs on match
            // but for failed searches, that becomes a long unbroken stretch
            yield;
          }
    }

    return { step, output };
  }

  const INTERVALS = [1, 5, 10, 20, 100, /* 200, */ 1000].reverse();

  const quantize_down = (x, step) => Math.floor(x / step) * step;
  const quantize_up = (x, step) => Math.ceil(x / step) * step;

  // get the number of years that marker band s should use for the given date boundaries
  function marker_interval_for(min, max) {
    const span = max - min;
    return INTERVALS.find(n => n < span) ?? INTERVALS[0];
  }

  function sorted_array(f) {
    return Object.assign([], {
      add(item) {
        const k = f(item);
        const i = this.findIndex(x => f(x) > k);
        this.splice(i === -1 ? this.length : i, 0, item);
      },
    });
  }

  // NO, no, no, no.  this can be sparse
  function make_tile_layer({ unit, onadd }) {
    const tiles = sorted_array(_ => _.iso);
    const container = document.createElement("div");
    container.classList.add("tile-layer");
    container.dataset.unit = unit;
    const _get = iso => tiles.find(tile => tile.iso === iso);
    const _has = iso => !!_get(iso);
    const _min = () => tiles.at(0)?.iso;
    const _max = () => tiles.at(-1)?.iso;
    function _add(iso) {
      if (!_has(iso)) {
        const element = document.createElement("div");
        element.classList.add("tile");
        element.dataset.time = iso;
        container.append(element);
        const tile = { iso, element };
        onadd?.(tile);
        tiles.add(tile);
      }
    }
    function _extend(to, from, step) {
      for (let it = from, c = 20; it !== to; it = step(it)) {
        if (!--c) throw new Error(`TTL: ${JSON.stringify({ from, to })}`);
        _add(it);
      }
    }
    function expose(iso) {
      if (acdh.acdh_type(iso) !== unit) throw new Error(`${iso} ∉ ${unit}`);
      if (tiles.length === 0) _add(iso);
      else if (iso < _min()) _extend(iso, _min(), acdh.iso_previous);
      else if (iso > _max()) _extend(iso, _max(), acdh.iso_next);
    }
    return { expose, container };
  }

  function make_timestream(container) {
    const EXT_R = 0.5;
    let last_state = null;

    const make_transition_band = (section_from, group_span) => band => {
      const year = band.year;
      const last_span = last_state.section_to - last_state.section_from;
      const last_r = (year - last_state.section_from) / last_span;
      const new_r = (year - section_from) / group_span;
      const x = (last_r * 100) / 2;
      const x2 = (new_r * 100) / 2;
      const C = 50; // use 0 for straight lines
      const exotic = x < 0 || x2 < 0 || x > 100 || x2 > 100; // mark connectors that go out of view
      // prettier-ignore
      const d1 = [
        `M ${x.toFixed(3)} 0`,
        `C ${x.toFixed(3)} ${C} ${x2.toFixed(3)} ${100-C} ${x2.toFixed(3)} 101`, // gapminder
      ];
      const d2 = [
        `M ${EXT} 0`,
        `L ${x.toFixed(3)} 0`,
        `C ${x.toFixed(3)} 40 ${x2.toFixed(3)} 60 ${x2.toFixed(3)} 101`, // gapminder
        // HACK to make out-of-bound shape less screwy (twisty)
        // “right” thing is to follow next band edge
        // either way, it bleeds outside of main area
        `L ${Math.max(EXT, x2)} 101`, // gapminder
        `Z`,
      ];
      return `<g data-year="${year}" class="timestream-band${
        exotic ? " exotic" : ""
      }">
          <path class="divider" d="${d1.join(" ")}" />
          <path class="shade" d="${d2.join(" ")}" />
        </g>`;
    };

    function make_transition_bands({
      section_from,
      group_span,
      from_bands,
      to_bands,
    }) {
      // sort prevents occlusion of later bands by earlier ones
      const paths = [...from_bands, ...to_bands]
        .sort((a, b) => (a.year < b.year ? -1 : a.year > b.year ? 1 : 0))
        .filter((band, i, all) => i === 0 || band.year !== all[i - 1].year) // deduplicate (sorted)
        .map(make_transition_band(section_from, group_span))
        .join(" ");
      return `<svg class="timestream-bands timestream-bands--transition" viewBox="0 0 100 100" preserveAspectRatio="none">${paths}</svg>`;
    }

    function straight_bands_svg(bands) {
      const content = bands
        .map(band => {
          const r = band.r / 2;
          const x = (r * 100).toFixed(3);
          // 101 height covers a sometime missing pixel at bottom
          return `<g data-year="${band.year}" class="timestream-band">
          <line class="divider" x1="${x}" x2="${x}" y1="0" y2="100" />
          <rect class="shade" x="${x}%" y="0"
            width="${((EXT_R - r) * 100).toFixed(3)}%"
            height="101"
          />
        </g>`;
        })
        .join("");

      return `<svg class="timestream-bands" viewBox="0 0 100 100" preserveAspectRatio="none">${content}</svg>`;
    }

    function section_controls() {
      return `<div data-part="controls">
        <button>remove</button>
        <button>open/close</button>
        <button>later</button>
        <button>earlier</button>
        <button>broader</button>
        <button>note/react</button>
        <button>download</button>
        <button data-command="layers">layers</button>
      </div>`;
    }

    let section_id = 0;
    function add_range_section({ earliest_year, latest_year }) {
      const section = document.createElement("section");
      section.classList.add("timestream-section");
      section_id++;
      section.dataset.section = section_id;

      const degenerate = earliest_year === latest_year;
      const marker_interval = degenerate
        ? 5
        : marker_interval_for(earliest_year, latest_year);
      const years_spanned = latest_year - earliest_year;
      const number_of_bands = Math.ceil(years_spanned / marker_interval);

      const this_section_bands = [...Array(number_of_bands)].map((_, i) => {
        const year = earliest_year + i * marker_interval;
        const r = (year - earliest_year) / years_spanned; // which should be just i / num groups
        return { r, year };
      });

      if (last_state?.group_bands.length > 0) {
        const transition = make_transition_bands({
          section_from: earliest_year,
          group_span: years_spanned,
          from_bands: last_state.group_bands,
          to_bands: this_section_bands,
        });
        container.insertAdjacentHTML("beforeend", transition);
      }

      last_state = {
        section_from: earliest_year,
        section_to: latest_year,
        group_bands: this_section_bands,
      };

      function year_tags(iso) {
        const year_wiki = iso; // but need padding
        const tag_map = links_from_map(year_wiki);
        const sorted = [...tag_map].sort(([, a], [, b]) =>
          a < b ? 1 : a > b ? -1 : 0
        );
        const top_tags = sorted.slice(0, 10);
        const max_hits = sorted.at(0)?.[1] ?? 1;
        const tags = Array.from(top_tags, ([wiki, hits]) => {
          return `<a class="tag" wiki="${wiki}" style="--hits: ${hits}">${decode_wiki_title(
            wiki
          )}</a>`;
        });
        return `<div class="tags" style="--max-hits: ${max_hits}">${tags.join(
          " ● "
        )}</div>`;
      }

      // The from-time and to-time attributes are provisional
      section.innerHTML = `
${section_controls()}
<figure data-part="timeline" data-from-time="${earliest_year}"  data-to-time="${latest_year}">
<figcaption>${earliest_year} – ${latest_year}</figcaption>
<div data-part="layers"></div>
</figure>`;

      function onadd(tile) {
        const { iso } = tile;
        const markup = year_tags(iso);
        tile.element.insertAdjacentHTML("beforeend", markup);
        tile.element.style.setProperty("--year", iso_to_year(iso));

        const label_html = `<a data-part="label" data-time="${iso}"><time datetime="${iso}">${iso}</time></a>`;
        tile.element.insertAdjacentHTML("afterbegin", label_html);
      }
      const layers_ele = section.querySelector(`[data-part="layers"]`);
      const year_layer = make_tile_layer({ unit: acdh.acdhut.Year, onadd });
      const markers_html = straight_bands_svg(this_section_bands);
      layers_ele.append(year_layer.container);
      year_layer.container.style.setProperty("--from-year", earliest_year);
      year_layer.container.style.setProperty("--to-year", latest_year);
      year_layer.container.insertAdjacentHTML("afterbegin", markers_html);
      year_layer.expose(earliest_year.toString());
      year_layer.expose(latest_year.toString());

      container.append(section);

      return section;
    }

    function add_notes_section({ earliest_year, latest_year, notes, caption }) {
      const section = document.createElement("section");
      section.classList.add("timestream-section");
      section_id++;
      section.dataset.section = section_id;

      const degenerate = earliest_year === latest_year;
      const marker_interval = degenerate
        ? 5
        : marker_interval_for(earliest_year, latest_year);
      const section_from = quantize_down(earliest_year, marker_interval);
      const section_to = quantize_up(latest_year, marker_interval);
      const group_span = section_to - section_from;
      const number_of_bands = Math.ceil(group_span / marker_interval);

      const this_section_bands = [...Array(number_of_bands)].map((_, i) => {
        const year = section_from + i * marker_interval;
        const r = (year - section_from) / group_span; // which should be just i / num groups
        return { r, year };
      });

      if (last_state?.group_bands.length > 0) {
        const transition = make_transition_bands({
          section_from,
          group_span,
          from_bands: last_state.group_bands,
          to_bands: this_section_bands,
        });
        container.insertAdjacentHTML("beforeend", transition);
      }

      last_state = {
        section_from,
        section_to,
        group_bands: this_section_bands,
      };

      const notes_markup = notes
        .map(note => {
          return `<li style="--year: ${iso_to_year(note.aboutTime)}"><a wiki="${
            note.wiki
          }"><time datetime="${note.aboutTime}">${iso_to_year(
            note.aboutTime
          )}</time></a> <span>${note.html}</span></li>`;
        })
        .join("");

      section.innerHTML = `
${section_controls()}
<figure data-part="timeline" style="--from-year: ${section_from}; --to-year: ${section_to}">
<figcaption>${caption}</figcaption>
<div data-part="layers">
  <ul class="timeline">${notes_markup}</ul>
</div>
</figure>`;

      container.append(section);
      return section;
    }

    return { add_notes_section, add_range_section };
  }

  const timestream_container = document.body.querySelector(
    `:scope > .timestream > [data-part="sections"]`
  );
  const timestream = make_timestream(timestream_container);

  const memoize1_Map =
    (f, cache = new Map()) =>
    x => {
      if (cache.has(x)) return cache.get(x);
      const y = f(x);
      cache.set(x, y);
      return y;
    };

  const search_kb_text = memoize1_Map(kb_text_search_process);

  function count_char(char, s) {
    let c = -1;
    for (let i = 0; i >= 0 && i < s.length; i = s.indexOf(char, i + 1)) ++c;
    return c;
  }

  function iso_to_year(iso) {
    if (iso.includes("/")); // todo: it's a millennium... so...
    if (iso.startsWith("-")) return -iso_to_year(iso.slice(1));
    // doesn't do bc, etc.  I know I have a function for this somewhere...
    if (iso.length === 2) return parseInt(iso, 10) * 100;
    return parseInt(iso, 10);
  }

  function go_up(message) {
    const n = count_char("p", message);
    const d = window.visualViewport.height * (n / -4);
    document.documentElement.scrollBy({ top: d, behavior: "smooth" });
    return "ok";
  }
  function go_down(message) {
    const n = count_char("n", message);
    const d = window.visualViewport.height * (n / 4);
    document.documentElement.scrollBy({ top: d, behavior: "smooth" });
    return "ok";
  }
  function help() {
    return "Tell ya whatcha can do";
  }
  function keep() {}
  function edit() {}
  function search(message) {
    const process = search_kb_text(message);
    system.add_sequence(process);
    document.body.querySelector(":scope > dialog").append(process.output);
    return `searching for ${message}`;
  }
  function go_to_record(record) {
    const MAX = 7;
    const { items } = record;
    const count = items.length;
    const truncated = count > MAX;
    const to_show = truncated ? items.slice(0, MAX) : items;

    const output = document.createElement("output");
    document.body.querySelector(":scope > dialog").append(output);
    output.innerHTML = `${truncated ? ` Here are the first ${MAX}` : ``}
<ol>
${to_show
  .map(note => {
    return `<li>
      <article>
      <p>In <cite><time><a wiki="${note.meta.wiki}">${
      note.meta.label
    }</a></time></cite>:</p>
        ${render_wiki_quote(record.wiki, note.html)}
      </article>
    </li>`;
  })
  .join("")}
</ol>
`;
    return `I have ${count} notes about ${record.label}!`;
  }

  function go_to_period(iso) {
    for (const record of kb.data)
      if (record.aboutTime === iso) return go_to_record(record);
  }
  function go_to_century(message) {
    return `Oh, I see, you want to visit a *century*?`;
  }
  function go_to_decade(message) {
    return `So, you want to go to a decade, huh?`;
  }
  function go_to_year(message) {
    const bc_mark = message.includes("-") || message.includes("b");
    const n = parseInt(message, 10);
    const year = bc_mark && n > 0 ? -n : n;
    const iso = `${year}`;
    go_to_period(iso);

    timestream.add_range_section({
      earliest_year: year - 2,
      latest_year: year + 2,
    });

    return `Okay, I will go to the year ${message}`;
  }

  const EXT = 50;

  function go_to_node(message) {
    const node = graph.get(message.toLocaleLowerCase());
    const output = document.createElement("output");
    document.body.querySelector(":scope > dialog").append(output);

    const transition_ele = document.createElement("div");
    document.body.querySelector(":scope > dialog").append(transition_ele);

    const MAX = 8;
    // only use notes at year level
    const notes = node.referencedBy.filter(_ => _.meta.aboutTime.length === 4);
    const truncate = notes.length > MAX;
    const format = note => {
      return `<li>
        <article>
          <p>
            <a wiki="${node.wiki}">${node.label}</a> is referenced in
            <cite
              >the article
              <a wiki="${note.wiki}"><time>${note.label}</time></a></cite
            >:
          </p>
          ${render_wiki_quote(note.wiki, note.html)}
        </article>
      </li>`;
    };

    const earliest_year = iso_to_year(notes.at(0).aboutTime);
    const latest_year = iso_to_year(notes.at(-1).aboutTime);
    const caption = `${notes.length} notes linking to “${message}”`;
    const section = timestream.add_notes_section({
      earliest_year,
      latest_year,
      notes,
      caption,
    });
    // mark links to the topic
    for (const link of [...section.querySelectorAll(`[wiki="${node.wiki}"]`)]) {
      const mark = link.ownerDocument.createElement("mark");
      link.parentNode.insertBefore(mark, link);
      mark.appendChild(link);
    }

    // prettier-ignore
    output.innerHTML =
          `<p>“${message}” is referenced ${node.hits} times.</p>
<p>The most recent note is in ${notes.at(-1).label}</p>
${notes.length < 2 ?"":`<p>The earliest in ${notes[0].label}.</p>`}
${truncate
  ? `<p>Here are ${MAX}:</p>
      <ol>${notes.slice(0, MAX/2).map(format).join("")}</ol>
      <p>(...jumping ahead...)</p>
      <ul>${notes.slice(-MAX/2).map(format).join("")}</ul>`
: `<ol>${notes.map(format).join("")}</ol>`
}`
    for (const link of output.querySelectorAll("a[wiki]"))
      if (link.getAttribute("wiki") === node.wiki) link.dataset.clicked = true;
  }

  const interpreters = [
    [
      /^\s*$/,
      () => {
        const r = 1 / graph.size;
        for (const key of graph.keys()) {
          if (Math.random() <= 2 * r) {
            const record = kb.data.find(
              record => record.wiki.toLocaleLowerCase() === key
            );
            if (record)
              return `Fine, how about ${key}?  ${go_to_record(record)}`;
          }
        }
      },
    ],
    [
      /^(?:(?:(?:s+e*a*r*c*h*\s*f*o*r*)|(?:f+i*n*d*))\s+)(?<term>.+)$/,
      (_, match) => search(match.groups.term),
    ],
    [
      { [Symbol.match]: () => Math.random() < 0.05 },
      () => {
        const A = [
          "Bruh",
          "No realeeeeee?",
          "You're done",
          "What uh",
          "🤙",
          "Just, stop",
          "You good?",
          "I'm good",
          "Sup",
          "Sus",
          "How's life",
          "That's cringe",
          "That's big brain",
          "You're big brain",
          "You are a person",
          "Ya think?",
          "Okaaaaay",
        ];
        return A[Math.floor(Math.random() * A.length)];
      },
    ],
    [
      /^(g+o*\s*t*o*)?\s*-?(first|second|third|fourth|fifth|sixth|seventh|1st|2nd|3rd|\dth|\d{1,2})\s*c+(e(n(t(u(r(y)?)?)?)?)?)?\s*(b+c*e*)?$/,
      go_to_century,
    ],
    [/^(g+o*\s*t*o*)?\s*-?\d{1,4}'?s\s*(b+c*e*)?$/, go_to_decade],
    [/^(g+o*\s*t*o*)?\s*-?\d{1,4}\s*(b+c*e*)?$/, go_to_year],
    [/^h+e*l*p*\s*m*e*$/i, help],
    [/^(go?\s*)?u*p+$/i, go_up],
    [/^((go?\s*)?d*o*w*)?n+$/i, go_down],
    [/^k+e*p*$/i, keep],
    [/^e+d*i*t*$/i, edit],
    [
      {
        [Symbol.match]: message => {
          return kb.data.find(record => record.label === message);
        },
      },
      (message, record) => {
        return `Yes, ${message} is a period I know of. ${go_to_record(record)}`;
      },
    ],
    [
      { [Symbol.match]: message => graph.has(message.toLocaleLowerCase()) },
      message => {
        go_to_node(message);

        const lo_key = message.toLocaleLowerCase();
        const others = [];
        const lo_key_space = ` ${lo_key}`;
        const space_lo_key = ` ${lo_key} `;
        for (const other of graph.keys()) {
          if (other === lo_key) continue;
          const other_lo_key = other.toLocaleLowerCase();
          if (
            other.startsWith(lo_key) ||
            other.includes(lo_key_space) ||
            others.includes(space_lo_key)
          )
            others.push(other);
        }
        if (others.length) {
          const output = document.createElement("output");
          document.body.querySelector(":scope > dialog").append(output);
          output.innerHTML = `<p>Or did you mean ${others
            .map(wiki => `<a wiki="${wiki}">${decode_wiki_title(wiki)}</a>`)
            .join(", or ")}</p>`;
        }

        const tag = Math.random() > 0.5 ? "I've heard of" : "Ah, yes,";
        return `${tag} ${message}.`;
      },
    ],
    [/^/, search],
  ];

  function interpret_0(message) {
    for (const [pattern, action] of interpreters) {
      const result = message.match(pattern);
      if (result) return action(message, result);
    }
  }

  function make_new_process_from_message(message) {
    // first_check_interpreters_for_match(message);
    return interpret_0(message);
  }

  function interpret(message) {
    const will_result_in_new_process = true;
    if (will_result_in_new_process) {
      const new_process = make_new_process_from_message(message);
      console.debug("NEW PROCESS", new_process);
      return new_process;
    } else {
      console.debug("No-op, presumably because such a process already existed");
    }
  }

  if (!globalThis.system)
    Object.defineProperty(globalThis, "system", { value: make_system() });

  const system = globalThis.system;
  system.play();

  function prompt(form) {
    // Attach to an existing dialog or create one
    // started using a pseudo dialog because I could not compete with its default style
    const dialog =
      document.body.querySelector(":scope > :is(dialog, .dialog)") ??
      document.body.appendChild(document.createElement("div"));
    dialog.classList.add("dialog");

    // adding the below event listeners to `dialog` is not working for some reason
    function check_scope(e) {
      do if (e === dialog) return true;
      while ((e = e.parentNode));
      return false;
    }

    globalThis.addEventListener("submit", event => {
      if (!check_scope(event.target)) return;
      event.preventDefault();

      {
        const form = event.target;
        const input = form.querySelector(`input:not([type="submit"])`);
        input.ariaLabel = "chat input";
        // HACK these are all kinds of bad
        input.autofocus = true;
        input.scrollIntoView({ behavior: "smooth" });
        const output = document.createElement("output");
        let result;
        try {
          result = interpret(input.value);
        } catch (error) {
          result = error.message;
        }
        if (result !== undefined) output.innerText = `${result}`;
        (form ?? dialog).appendChild(output);
      }

      {
        const form = document.createElement("form");
        form.innerHTML = `<fieldset>
          <input type="submit" value="▶" tabindex="-1" />
          <input autofocus aria-label="chat input" />
        </fieldset>`;
        dialog.appendChild(form);
        form.querySelector(`input:not([type="submit"])`).focus();
      }
    });

    globalThis.addEventListener("click", event => {
      const lang = "en";
      const command = event.target.getAttribute("data-command");
      if (command) {
        const section_ele = event.target.closest("[data-section]");
        if (!section_ele)
          return console.warn(`You want to ${command}, but what section?`);
        const section = section_ele.getAttribute("data-section");
        if (command === "layers") {
          section_ele.classList.toggle("show-layers");
          return;
        } else return console.warn(`I don't know how to ${command}`);
        return;
      }

      const wiki_link = event.target.closest("a[wiki]");
      if (wiki_link) {
        wiki_link.dataset.clicked = true;
        const wiki = wiki_link.getAttribute("wiki");
        if (event.ctrlKey) window.open(wiki_url_for(wiki, { lang }), "_blank");
        else {
          const form = dialog.querySelector(":scope > form:last-of-type");
          if (form) {
            const input = form.querySelector(`input:not([type="submit"])`);
            if (input) {
              input.value = decode_wiki_title(wiki);
              form.requestSubmit();
              event.preventDefault();
            }
          }
        }
      }
    });

    if (dialog instanceof HTMLDialogElement) dialog.show();
  }

  setTimeout(() => system.add_sequence(preprocess_items()), 1000);
  prompt();
})();
