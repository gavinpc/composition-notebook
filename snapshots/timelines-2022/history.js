(() => {
  function make_system() {
    let _lifeline;
    const _sequences = new Map();
    const _completed = new Set();
    const performance_now = performance.now.bind(performance);

    function step(budget = 10, now = performance_now) {
      _lifeline = null;
      if (_sequences.size === 0) return;
      const [[thing, { iterator }]] = _sequences;
      let tally = 0;
      let begin = now();
      const stop_by = begin + budget;
      while (begin < stop_by) {
        const result = iterator.next();
        const end = now();
        ++tally;
        if (result.done) {
          _sequences.delete(thing);
          _completed.add(thing);
          if (_sequences.size === 0) return;
          break;
        }
        const cost = end - begin;
        if (cost > budget) {
          console.warn(
            `Single step took ${cost}ms, more than budget of ${budget}`
          );
          break;
        }
        begin = end;
      }

      setTimeout(play, 10);
    }
    function play() {
      _lifeline ??= requestAnimationFrame(step);
    }
    function pause() {
      if (_lifeline) {
        cancelAnimationFrame(_lifeline);
        _lifeline = null;
      }
    }
    function add_sequence(thing) {
      if (_sequences.has(thing) || _completed.has(thing)) return;
      if (typeof thing.step !== "function")
        throw new Error(`System expected something with a ‘step’ function.`);
      if (thing.step.length !== 0)
        throw new Error(
          `System expected step function to accept no arguments.`
        );
      const iterator = thing.step();
      if (typeof iterator.next !== "function")
        throw new Error(`System expected step function to return an iterable.`);

      _sequences.set(thing, { thing, iterator });
      play();
    }
    // get playing() { return !!_lifeline; }
    return { play, pause, add_sequence, _completed, _sequences };
  }

  // https://github.com/tc39/proposal-regex-escaping/blob/main/polyfill.js
  // seeAlso https://github.com/tc39/proposal-regex-escaping/issues/37
  // This is safe for whole patterns.  The objections are to certain joins.
  const RegExp_escape = s => String(s).replace(/[\\^$*+?.()|[\]{}]/g, "\\$&");

  const int = s => parseInt(s, 0);
  const pad = len => n =>
    (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");
  const pad4 = pad(4);

  const decode_wiki_title = s => decodeURIComponent(s.replace(/_/g, " "));

  const graph = new Map();
  Object.defineProperty(globalThis, "graph", { value: graph }); // for console

  function preprocess_item(meta, data) {
    if (!("meta" in data)) data.meta = meta;
    if (!("text" in data)) {
      // DOMParser creates a whole HtmlDocument.
      const ele = document.createElement("div");
      ele.innerHTML = data.html;

      // BUT this includes `items`, which makes no sense
      const note = { ...meta, ...data };
      // Build graph nodes from references.  It's completely unrelated to
      // setting the record's text, but since we have an element to query...
      for (const wiki_link of ele.querySelectorAll(`a[wiki]`)) {
        const wiki = wiki_link.getAttribute("wiki");
        const label = decode_wiki_title(wiki);
        const key = label.toLocaleLowerCase();
        let node = graph.get(key);
        if (node) {
          node.wiki = wiki;
          node.label = label;
          node.referencedBy.push(note);
          node.hits++;
        } else {
          graph.set(
            key,
            (node = { wiki, label, referencedBy: [note], hits: 1 })
          );
        }
      }

      data.text = ele.innerText;
    }
  }

  function record_for(year) {
    const time = year.toString();
    return kb.data.find(record => record.aboutTime === time);
  }

  function is_period_wiki(wiki) {
    const it = wiki.trim().toLowerCase();
    return kb.data.find(record => record.wiki.toLowerCase() === it);
  }

  function* links_from(year) {
    for (const item of record_for(year)?.items || []) {
      const ele = document.createElement("div");
      ele.innerHTML = item.html;
      for (const wiki_link of ele.querySelectorAll(`a[wiki]`)) {
        const wiki = wiki_link.getAttribute("wiki");
        // TODO: also filter out months & days of year
        if (!is_period_wiki(wiki)) yield wiki;
      }
    }
  }

  function links_from_map(year) {
    const map = new Map();
    for (const wiki of links_from(year))
      if (map.has(wiki)) map.set(wiki, map.get(wiki) + 1);
      else map.set(wiki, 1);
    return map;
  }

  function preprocess_items() {
    function* step() {
      for (const record of kb.data)
        for (const item of record.items) {
          preprocess_item(record, item);
          yield;
        }
    }
    return { step };
  }

  function wiki_url_for(name, opts) {
    const lang = opts?.lang ?? "en";
    const m = opts?.mobile ? "m." : "";
    return `https://${lang}.${m}wikipedia.org/wiki/${name}`;
  }

  function render_wiki_quote(wiki, markup) {
    // prettier-ignore
    return `<blockquote cite="${wiki_url_for(wiki, { lang: "en" })}">${markup}</blockquote>`;
  }

  function kb_text_search_process(needle, doc = document) {
    const output = doc.createElement("output");
    output.dataset.query = needle;

    function* step() {
      let match_count = 0;
      const pattern = RegExp_escape(needle);
      const flags = needle === needle.toLocaleLowerCase() ? "i" : "";
      const regex = new RegExp(pattern, `g${flags}`);
      const p = (record, item) => {
        preprocess_item(record, item);
        return regex.test(item.text);
      };
      // map pin markers https://stackoverflow.com/a/54331400

      for (const record of kb.data)
        if (record.items)
          for (const data of record.items) {
            if (p(record, data)) {
              if (++match_count >= 8) break; // else we blow up.  but who should do this?
              // this skirts 2 tricky problems:
              // - match the text, not the markup
              // - wrap the the text (not the markup) spanning/balancing elements
              // See https://github.com/julmot/mark.js
              const markup = data.html.replace(regex, "<mark>$&</mark>");
              // prettier-ignore
              output.innerHTML += `<article>
                <header aria-role="contentinfo">
                  <p>
                    This item from
                    <cite>
                      an article about <a wiki="${record.wiki}"><time>${record.label}</time></a>
                    </cite>
                    matches “${needle}”
                  </p>
                </header>
                ${render_wiki_quote(record.wiki, markup)}
              </article>`;
            }
            // more overhead to yield here vs on match
            // but for failed searches, that becomes a long unbroken stretch
            yield;
          }
    }

    return { step, output };
  }

  const INTERVALS = [1, 5, 10, 20, 100, /* 200, */ 1000].reverse();

  const quantize_down = (x, step) => Math.floor(x / step) * step;
  const quantize_up = (x, step) => Math.ceil(x / step) * step;

  // Inclusively iterate the ISO periods from `first` to `last`,
  // starting at the granularity that yields no more than MAX items
  // THEN all of the broader periods that include them.
  // assumes that delta will not increase (at least, not beyond max)
  function* covering_periods(first, last, MAX = 12) {
    const delta = ISO8601.subtract(last, first);
    if (!delta) throw new Error(`Can't cover ${first}–${last}`);
    const n = delta.n + 1;
    if (n <= MAX) {
      let it = first;
      let i = 0;
      while (true) {
        yield it;
        i++;
        if (i >= n) {
          console.assert(it === last, `cover ${first}–${last} ended @ ${it}`);
          break;
        }
        it = ISO8601.next(it);
      }
      // eschew this shorter version to check the invariant
      // for (let i = 0, it = from; i < n; i++, it = ISO8601.next(it)) yield it;
    }
    const first_up = ISO8601.broader(first);
    const last_up = ISO8601.broader(last);
    if (first_up && last_up) yield* covering_periods(first_up, last_up, MAX);
  }

  // get the number of years that marker band s should use for the given date boundaries
  function marker_interval_for(min, max) {
    const span = max - min;
    return INTERVALS.find(n => n < span) ?? INTERVALS[0];
  }

  // note that tiles are not logically ordered (either in storage or document)
  function make_tile_layer({ unit, onadd }) {
    const tiles = new Map();
    const container = document.createElement("div");
    container.dataset.tilesOf = unit;
    function expose(iso) {
      const parsed = ISO8601.parse(iso);
      if (parsed?.type !== unit) throw new Error(`${iso}∉${unit}`);
      if (tiles.has(iso)) return;
      const element = document.createElement("div");
      element.classList.add("tile");
      let value = iso;
      if (parsed.type === "Millennium")
        value = (parsed.millennium / 1000 - 1).toFixed();
      element.style.setProperty(`--${unit.toLowerCase()}`, value);
      container.append(element);
      const tile = { iso, element };
      onadd?.(tile);
      tiles.set(iso, tile);
    }
    return { expose, container };
  }

  const ANNUALS = ["Millennium", "Century", "Decade", "Year"];
  function make_tile_layers({ container, onaddtile }) {
    const layers = Object.fromEntries(
      ANNUALS.map(unit => {
        const onadd = tile => onaddtile({ unit, tile });
        const layer = make_tile_layer({ unit, onadd });
        container.append(layer.container);
        return [unit, layer];
      })
    );
    function expose(iso) {
      const type = ISO8601.parse(iso)?.type;
      const layer = layers[type];
      if (!layer) return console.warn(`No layer for ${iso} of type ‘${type}’`);
      layer.expose(iso);
    }
    function focus(unit) {
      container.dataset.timeFocus = unit;
    }
    return { expose, focus };
  }

  function make_timestream(container) {
    const lang = "en";
    const EXT_R = 0.5;
    let last_state = null;

    const make_transition_band = (section_from, group_span) => band => {
      const year = band.year;
      const last_span = last_state.section_to - last_state.section_from;
      const last_r = (year - last_state.section_from) / last_span;
      const new_r = (year - section_from) / group_span;
      const x = (last_r * 100) / 2;
      const x2 = (new_r * 100) / 2;
      const C = 50; // use 0 for straight lines
      const exotic = x < 0 || x2 < 0 || x > 100 || x2 > 100; // mark connectors that go out of view
      // prettier-ignore
      const d1 = [
        `M ${x.toFixed(3)} 0`,
        `C ${x.toFixed(3)} ${C} ${x2.toFixed(3)} ${100-C} ${x2.toFixed(3)} 101`, // gapminder
      ];
      const d2 = [
        `M ${EXT} 0`,
        `L ${x.toFixed(3)} 0`,
        `C ${x.toFixed(3)} 40 ${x2.toFixed(3)} 60 ${x2.toFixed(3)} 101`, // gapminder
        // HACK to make out-of-bound shape less screwy (twisty)
        // “right” thing is to follow next band edge
        // either way, it bleeds outside of main area
        `L ${Math.max(EXT, x2)} 101`, // gapminder
        `Z`,
      ];
      return `<g data-year="${year}" class="timestream-band${
        exotic ? " exotic" : ""
      }">
          <path class="divider" d="${d1.join(" ")}" />
          <path class="shade" d="${d2.join(" ")}" />
        </g>`;
    };

    function make_transition_bands({
      section_from,
      group_span,
      from_bands,
      to_bands,
    }) {
      // sort prevents occlusion of later bands by earlier ones
      const paths = [...from_bands, ...to_bands]
        .sort((a, b) => (a.year < b.year ? -1 : a.year > b.year ? 1 : 0))
        .filter((band, i, all) => i === 0 || band.year !== all[i - 1].year) // deduplicate (sorted)
        .map(make_transition_band(section_from, group_span))
        .join(" ");
      return `<svg class="timestream-bands timestream-bands--transition" viewBox="0 0 100 100" preserveAspectRatio="none">${paths}</svg>`;
    }

    function straight_year_bands_svg(bands) {
      const content = bands
        .map(band => {
          const r = band.r / 2;
          const x = (r * 100).toFixed(3);
          // 101 height covers a sometime missing pixel at bottom
          return `<g data-year="${band.year}" class="timestream-band">
          <line class="divider" x1="${x}" x2="${x}" y1="0" y2="100" />
          <rect class="shade" x="${x}%" y="0"
            width="${((EXT_R - r) * 100).toFixed(3)}%"
            height="101"
          />
        </g>`;
        })
        .join("");

      return `<svg class="timestream-bands" viewBox="0 0 100 100" preserveAspectRatio="none">${content}</svg>`;
    }

    function section_controls() {
      return `<div data-part="controls">
        <button>remove</button>
        <button>open/close</button>
        <button>later</button>
        <button>earlier</button>
        <button>broader</button>
        <button>note/react</button>
        <button>download</button>
        <button data-command="layers">layers</button>
      </div>`;
    }

    function year_tags(iso) {
      const year_wiki = iso; // but need padding
      const tag_map = links_from_map(year_wiki);
      const sorted = [...tag_map].sort(([, a], [, b]) =>
        a < b ? 1 : a > b ? -1 : 0
      );
      const top_tags = sorted.slice(0, 10);
      const max_hits = sorted.at(0)?.[1] ?? 1;
      const tags = Array.from(top_tags, ([wiki, hits]) => {
        return `<a class="tag" wiki="${wiki}" style="--hits: ${hits}">${decode_wiki_title(
          wiki
        )}</a>`;
      });
      return `<div class="tags" style="--max-hits: ${max_hits}">${tags.join(
        " ● "
      )}</div>`;
    }

    function datetime_link(iso, lang) {
      const link = document.createElement("a");
      link.dataset.datetime = iso;
      const label = ISO8601.label(iso, lang);
      link.innerHTML = `<time datetime="${iso}">${label}</time>`;
      return link;
    }

    let section_id = 0;
    function add_range_section({ earliest_year, latest_year }) {
      const section = document.createElement("section");
      section.classList.add("timestream-section");
      section_id++;
      section.dataset.section = section_id;

      const degenerate = earliest_year === latest_year;
      const marker_interval = degenerate
        ? 5
        : marker_interval_for(earliest_year, latest_year);
      const years_spanned = latest_year - earliest_year;
      const number_of_bands = Math.ceil(years_spanned / marker_interval);

      const this_section_bands = [...Array(number_of_bands)].map((_, i) => {
        const year = earliest_year + i * marker_interval;
        const r = (year - earliest_year) / years_spanned; // which should be just i / num groups
        return { r, year };
      });

      if (last_state?.group_bands.length > 0) {
        const transition = make_transition_bands({
          section_from: earliest_year,
          group_span: years_spanned,
          from_bands: last_state.group_bands,
          to_bands: this_section_bands,
        });
        container.insertAdjacentHTML("beforeend", transition);
      }

      last_state = {
        section_from: earliest_year,
        section_to: latest_year,
        group_bands: this_section_bands,
      };

      // The from-time and to-time attributes are provisional
      section.innerHTML = `
${section_controls()}
<figure data-part="timeline" data-from-time="${earliest_year}"  data-to-time="${latest_year}">
<figcaption>${earliest_year} – ${latest_year}</figcaption>
<div data-part="layers"></div>
</figure>`;
      section.style.setProperty("--from-year", earliest_year);
      section.style.setProperty("--to-year", int(latest_year) + 1);

      const layers_ele = section.querySelector(`[data-part="layers"]`);
      const tile_layers = make_tile_layers({
        container: layers_ele,
        onaddtile({ tile }) {
          const { iso } = tile;
          const label_link = datetime_link(iso, lang);
          tile.element.insertAdjacentElement("afterbegin", label_link);
          const markup = year_tags(iso);
          tile.element.insertAdjacentHTML("beforeend", markup);
          tile.element.style.setProperty("--year", iso_to_year(iso));
          label_link.dataset.part = "label";
          tile.element.insertAdjacentElement("afterbegin", label_link);
        },
      });

      const first = pad4(earliest_year);
      const last = pad4(latest_year);
      for (const iso of covering_periods(first, last)) tile_layers.expose(iso);

      container.append(section);

      return section;
    }

    function add_notes_section({ earliest_year, latest_year, notes, caption }) {
      const section = document.createElement("section");
      section.classList.add("timestream-section");
      section_id++;
      section.dataset.section = section_id;

      const degenerate = earliest_year === latest_year;
      const marker_interval = degenerate
        ? 5
        : marker_interval_for(earliest_year, latest_year);
      const section_from = quantize_down(earliest_year, marker_interval);
      const section_to = quantize_up(latest_year, marker_interval);
      const group_span = section_to - section_from;
      const number_of_bands = Math.ceil(group_span / marker_interval);

      const this_section_bands = [...Array(number_of_bands)].map((_, i) => {
        const year = section_from + i * marker_interval;
        const r = (year - section_from) / group_span; // which should be just i / num groups
        return { r, year };
      });

      if (last_state?.group_bands.length > 0) {
        const transition = make_transition_bands({
          section_from,
          group_span,
          from_bands: last_state.group_bands,
          to_bands: this_section_bands,
        });
        container.insertAdjacentHTML("beforeend", transition);
      }

      last_state = {
        section_from,
        section_to,
        group_bands: this_section_bands,
      };

      const notes_markup = notes
        .map(note => {
          return `<li style="--year: ${iso_to_year(
            note.aboutTime
          )}"><a data-datetime="${note.aboutTime}"><time datetime="${
            note.aboutTime
          }">${ISO8601.label(note.aboutTime, lang)}</time></a> <span>${
            note.html
          }</span></li>`;
        })
        .join("");

      section.innerHTML = `
${section_controls()}
<figure data-part="timeline">
<figcaption>${caption}</figcaption>
<div data-part="layers"></div>
</figure>`;
      section.style.setProperty("--from-year", earliest_year);
      section.style.setProperty("--to-year", int(latest_year) + 1);

      const layers_ele = section.querySelector(`[data-part="layers"]`);
      const tile_layers = make_tile_layers({
        container: layers_ele,
        onaddtile({ tile }) {
          const { iso } = tile;
          const label_link = datetime_link(iso, lang);
          label_link.dataset.part = "label";
          tile.element.insertAdjacentElement("afterbegin", label_link);

          const tags_markup = year_tags(iso);
          tile.element.insertAdjacentHTML("beforeend", tags_markup);
        },
      });

      const first = pad4(earliest_year);
      const last = pad4(latest_year);
      for (const iso of covering_periods(first, last)) tile_layers.expose(iso);

      // So that this appears after layers (assumes above adds initial tiles synchronously)
      layers_ele.insertAdjacentHTML(
        "beforeend",
        `<ul class="timeline">${notes_markup}</ul>`
      );

      container.append(section);
      return section;
    }

    return { add_notes_section, add_range_section };
  }

  const timestream_container = document.body.querySelector(
    `:scope > .timestream > [data-part="sections"]`
  );
  const timestream = make_timestream(timestream_container);

  const memoize1_Map =
    (f, cache = new Map()) =>
    x => {
      if (cache.has(x)) return cache.get(x);
      const y = f(x);
      cache.set(x, y);
      return y;
    };

  const search_kb_text = memoize1_Map(kb_text_search_process);

  function count_char(char, s) {
    let c = -1;
    for (let i = 0; i >= 0 && i < s.length; i = s.indexOf(char, i + 1)) ++c;
    return c;
  }

  function iso_to_year(iso) {
    if (iso.includes("/")); // todo: it's a millennium... so...
    if (iso.startsWith("-")) return -iso_to_year(iso.slice(1));
    // doesn't do bc, etc.  I know I have a function for this somewhere...
    if (iso.length === 2) return int(iso) * 100;
    return int(iso);
  }

  function go_up(message) {
    const n = count_char("p", message);
    const d = window.visualViewport.height * (n / -4);
    document.documentElement.scrollBy({ top: d, behavior: "smooth" });
    return "ok";
  }
  function go_down(message) {
    const n = count_char("n", message);
    const d = window.visualViewport.height * (n / 4);
    document.documentElement.scrollBy({ top: d, behavior: "smooth" });
    return "ok";
  }
  function help() {
    return "Tell ya whatcha can do";
  }
  function keep() {}
  function edit() {}
  function search(message) {
    const process = search_kb_text(message);
    system.add_sequence(process);
    document.body.querySelector(":scope > dialog").append(process.output);
    return `searching for ${message}`;
  }
  /*
  function go_to_record(record) {
    const MAX = 7;
    const { items } = record;
    const count = items.length;
    const truncated = count > MAX;
    const to_show = truncated ? items.slice(0, MAX) : items;

    const output = document.createElement("output");
    document.body.querySelector(":scope > dialog").append(output);
    output.innerHTML = `${truncated ? ` Here are the first ${MAX}` : ``}
<ol>
${to_show
  .map(note => {
    return `<li>
      <article>
      <p>In <cite><a wiki="${note.meta.wiki}"><time datetime="${
      note.meta.aboutTime
    }">${note.meta.label}</time></a></cite>:</p>
        ${render_wiki_quote(record.wiki, note.html)}
      </article>
    </li>`;
  })
  .join("")}
</ol>
`;
    return `I have ${count} notes about ${record.label}!`;
  }
*/

  function go_to_period(first, last) {
    // for (const record of kb.data)
    // if (record.aboutTime === iso) return go_to_record(record);

    timestream.add_range_section({
      earliest_year: first,
      latest_year: last,
    });
  }
  function go_to_century(message) {
    return `Oh, I see, you want to visit a *century*?`;
  }
  function go_to_decade(message) {
    return `So, you want to go to a decade, huh?`;
  }
  function go_to_year(message) {
    const bc_mark = message.includes("-") || message.includes("b");
    const n = int(message);
    const year = bc_mark && n > 0 ? -n : n;
    const iso = `${year}`;
    go_to_period(pad4(year - 2), pad4(year + 2));

    return `Okay, I will go to the year ${message}`;
  }

  const EXT = 50;

  function go_to_node(message) {
    const node = graph.get(message.toLocaleLowerCase());
    const output = document.createElement("output");
    document.body.querySelector(":scope > dialog").append(output);

    const transition_ele = document.createElement("div");
    document.body.querySelector(":scope > dialog").append(transition_ele);

    const MAX = 8;
    // only use notes at year level
    const notes = node.referencedBy.filter(_ => _.meta.aboutTime.length === 4);
    const truncate = notes.length > MAX;
    const format = note => {
      return `<li>
        <article>
          <p>
            <a wiki="${node.wiki}">${node.label}</a> is referenced in
            <cite
              >the article
              <a wiki="${note.wiki}"><time>${note.label}</time></a></cite
            >:
          </p>
          ${render_wiki_quote(note.wiki, note.html)}
        </article>
      </li>`;
    };

    const earliest_year = iso_to_year(notes.at(0).aboutTime);
    const latest_year = iso_to_year(notes.at(-1).aboutTime);
    const caption = `${notes.length} notes linking to “${message}”`;
    const section = timestream.add_notes_section({
      earliest_year,
      latest_year,
      notes,
      caption,
    });
    // mark links to the topic
    for (const link of [...section.querySelectorAll(`[wiki="${node.wiki}"]`)]) {
      const mark = link.ownerDocument.createElement("mark");
      link.parentNode.insertBefore(mark, link);
      mark.appendChild(link);
    }

    // prettier-ignore
    output.innerHTML =
          `<p>“${message}” is referenced ${node.hits} times.</p>
<p>The most recent note is in ${notes.at(-1).label}</p>
${notes.length < 2 ?"":`<p>The earliest in ${notes[0].label}.</p>`}
${truncate
  ? `<p>Here are ${MAX}:</p>
      <ol>${notes.slice(0, MAX/2).map(format).join("")}</ol>
      <p>(...jumping ahead...)</p>
      <ul>${notes.slice(-MAX/2).map(format).join("")}</ul>`
: `<ol>${notes.map(format).join("")}</ol>`
}`
    for (const link of output.querySelectorAll("a[wiki]"))
      if (link.getAttribute("wiki") === node.wiki) link.dataset.clicked = true;
  }

  const random_item_from = a => a[Math.floor(Math.random() * a.length)];

  const interpreters = [
    [/^\s*$/, () => `what, should I pick a random topic?`],
    [
      /^(?:(?:(?:s+e*a*r*c*h*\s*f*o*r*)|(?:f+i*n*d*))\s+)(?<term>.+)$/,
      (_, match) => search(match.groups.term),
    ],
    [
      { [Symbol.match]: () => Math.random() < 0.05 },
      () => {
        const A = [
          "Bruh",
          "No realeeeeee?",
          "You're done",
          "What uh",
          "🤙",
          "Just, stop",
          "You good?",
          "I'm good",
          "Sup",
          "Sus",
          "How's life",
          "That's cringe",
          "That's big brain",
          "You're big brain",
          "You are a person",
          "Ya think?",
          "Chill, my guy",
          "Okaaaaay",
        ];
        return A[Math.floor(Math.random() * A.length)];
      },
    ],
    [
      /^(g+o*\s*t*o*)?\s*-?(first|second|third|fourth|fifth|sixth|seventh|1st|2nd|3rd|\dth|\d{1,2})\s*c+(e(n(t(u(r(y)?)?)?)?)?)?\s*(b+c*e*)?$/,
      go_to_century,
    ],
    [/^(g+o*\s*t*o*)?\s*-?\d{1,4}'?s\s*(b+c*e*)?$/, go_to_decade],
    [/^(g+o*\s*t*o*)?\s*-?\d{1,4}\s*(b+c*e*)?$/, go_to_year],
    [/^h+e*l*p*\s*m*e*$/i, help],
    [/^(go?\s*)?u*p+$/i, go_up],
    [/^((go?\s*)?d*o*w*)?n+$/i, go_down],
    [/^k+e*p*$/i, keep],
    [/^e+d*i*t*$/i, edit],
    [
      {
        [Symbol.match]: message => {
          return kb.data.find(record => record.label === message);
        },
      },
      message => {
        return `Yes, ${message} is a period I know of.`;
      },
    ],
    [
      { [Symbol.match]: message => graph.has(message.toLocaleLowerCase()) },
      message => {
        go_to_node(message);

        const lo_key = message.toLocaleLowerCase();
        const others = [];
        const lo_key_space = ` ${lo_key}`;
        const space_lo_key = ` ${lo_key} `;
        for (const other of graph.keys()) {
          if (other === lo_key) continue;
          const other_lo_key = other.toLocaleLowerCase();
          if (
            other.startsWith(lo_key) ||
            other.includes(lo_key_space) ||
            others.includes(space_lo_key)
          )
            others.push(other);
        }
        if (others.length) {
          const output = document.createElement("output");
          document.body.querySelector(":scope > dialog").append(output);
          output.innerHTML = `<p>Or did you mean ${others
            .map(wiki => `<a wiki="${wiki}">${decode_wiki_title(wiki)}</a>`)
            .join(", or ")}</p>`;
        }

        const tag = Math.random() > 0.5 ? "I've heard of" : "Ah, yes,";
        return `${tag} ${message}.`;
      },
    ],
    [/^/, search],
  ];

  function interpret_0(message) {
    for (const [pattern, action] of interpreters) {
      const result = message.match(pattern);
      if (result) return action(message, result);
    }
  }

  function make_new_process_from_message(message) {
    // first_check_interpreters_for_match(message);
    return interpret_0(message);
  }

  function interpret(message) {
    const will_result_in_new_process = true;
    if (will_result_in_new_process) {
      const new_process = make_new_process_from_message(message);
      console.debug("NEW PROCESS", new_process);
      return new_process;
    } else {
      console.debug("No-op, presumably because such a process already existed");
    }
  }

  if (!globalThis.system)
    Object.defineProperty(globalThis, "system", { value: make_system() });

  const system = globalThis.system;
  system.play();

  function prompt(form) {
    // Attach to an existing dialog or create one
    // started using a pseudo dialog because I could not compete with its default style
    const dialog =
      document.body.querySelector(":scope > :is(dialog, .dialog)") ??
      document.body.appendChild(document.createElement("div"));
    dialog.classList.add("dialog");

    // adding the below event listeners to `dialog` is not working for some reason
    function check_scope(e) {
      do if (e === dialog) return true;
      while ((e = e.parentNode));
      return false;
    }

    globalThis.addEventListener("submit", event => {
      if (!check_scope(event.target)) return;
      event.preventDefault();

      {
        const form = event.target;
        const input = form.querySelector(`input:not([type="submit"])`);
        input.ariaLabel = "chat input";
        // HACK these are all kinds of bad
        input.autofocus = true;
        input.scrollIntoView({ behavior: "smooth" });
        const output = document.createElement("output");
        let result;
        try {
          result = interpret(input.value);
        } catch (error) {
          result = error.message;
        }
        if (result !== undefined) output.innerText = `${result}`;
        (form ?? dialog).appendChild(output);
      }

      {
        const form = document.createElement("form");
        form.innerHTML = `<fieldset>
          <input type="submit" value="▶" tabindex="-1" />
          <input autofocus aria-label="chat input" />
        </fieldset>`;
        dialog.appendChild(form);
        form.querySelector(`input:not([type="submit"])`).focus();
      }
    });

    globalThis.addEventListener("click", event => {
      const lang = "en";
      const command = event.target.getAttribute("data-command");
      if (command) {
        const section_ele = event.target.closest("[data-section]");
        if (!section_ele)
          return console.warn(`You want to ${command}, but what section?`);
        const section = section_ele.getAttribute("data-section");
        if (command === "layers") {
          section_ele.classList.toggle("show-layers");
          return;
        } else return console.warn(`I don't know how to ${command}`);
        return;
      }

      const time_link = event.target.closest("a[data-datetime]");
      if (time_link) {
        const target_time = time_link.dataset.datetime;
        console.log(`Okay, I will try to go to ${target_time}`);
        return;
      }

      const wiki_link = event.target.closest("a[wiki]");
      if (wiki_link) {
        wiki_link.dataset.clicked = true;
        const wiki = wiki_link.getAttribute("wiki");
        if (event.ctrlKey) window.open(wiki_url_for(wiki, { lang }), "_blank");
        else {
          const form = dialog.querySelector(":scope > form:last-of-type");
          if (form) {
            const input = form.querySelector(`input:not([type="submit"])`);
            if (input) {
              input.value = decode_wiki_title(wiki);
              form.requestSubmit();
              event.preventDefault();
            }
          }
        }
      }
    });

    if (dialog instanceof HTMLDialogElement) dialog.show();
  }

  setTimeout(() => system.add_sequence(preprocess_items()), 1000);
  prompt();
})();
