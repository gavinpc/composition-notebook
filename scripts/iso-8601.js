(function () {
  const int = s => parseInt(s, 10);

  const pad = len => n =>
    (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");

  const pad2 = pad(2);
  const pad3 = pad(3);
  const pad4 = pad(4);

  // ISO8601 does not have a concept of millennium as such; we follow ACDH here
  const ISO8601 = (() => {
    const MILLENNIUM = /^((-?)\d{4,5})\/-?(\2\d0000?)$/;
    const DATE =
      /^(?<day>(?<month>(?<year>(?<decade>(?<bc>-?)(?<century>\d\d)(?<d>\d)?)(?<y>\d)?)(?:-(?<mm>\d\d))?)(?:-(?<dd>\d\d))?)$/;

    const range = (n, f) => Array.from(Array(n).keys(), f);

    function parse(iso) {
      if (typeof iso !== "string") return;

      // e.g. 2nd millennium is https://vocabs.acdh.oeaw.ac.at/date/1001/2000
      const mill = iso.match(MILLENNIUM);
      if (mill) {
        const [, from, bc, to] = mill;
        const n = int(to);
        if (n !== int(from) + 999)
          throw new Error(`Ill-formed millennium (999-wise) ${iso}`);
        // `millennium` is the ending year (so, 1000 for first millennium)
        return { input: iso, type: "Millennium", bc, millennium: n };
      }

      const match = iso.match(DATE);
      if (!match) return;

      const { dd, mm, y, d } = match.groups;
      // prettier-ignore
      const type = dd ? "Day"
          : mm ? "Month"
          : y ? "Year"
          : d ? "Decade"
          : "Century"

      return { input: iso, type, ...match.groups };
    }

    function broader(iso) {
      const parsed = parse(iso);
      if (!parsed) return;
      const { type } = parsed;
      if (type === "Century") {
        const m = Math.floor(int(parsed.century) / 10);
        return `${parsed.bc}${m}001/${parsed.bc}${m + 1}000`;
      }
      // Century is off-by-one from that of their “narrower” decades.
      // i.e. Century 20 contains decades 190, 191... etc.
      if (type === "Decade")
        return pad2((parsed.bc ? -1 : 1) * (int(parsed.century) + 1));
      if (type === "Year") return parsed.decade;
      if (type === "Month") return parsed.year;
      if (type === "Day") return parsed.month;
    }

    function narrower(iso) {
      const parsed = parse(iso);
      if (!parsed) return undefined;

      // Millennium => Century*
      if (parsed.type === "Millennium") {
        const n = int(parsed.millennium) / 1000;
        if (n !== Math.floor(n)) throw new Error(`BAD MILLENNIUM ${iso}`);
        return range(10, i => pad2((n - 1) * 10 + i + (parsed.bc ? 0 : 1)));
      }

      // Century => Decade*
      if (parsed.type === "Century") {
        const n = int(parsed.century);
        const nn = parsed.bc ? n + 1 : n - 1; // centuries are off by one... RIGHT??
        const base = parsed.bc + pad2(Math.abs(nn));
        return range(10, i => `${base}${i}`);
      }

      // Decade => Year*
      if (parsed.type === "Decade")
        return range(10, i => `${parsed.input}${i}`);

      // Year => Month*
      if (parsed.type === "Year")
        return range(12, i => `${parsed.input}-${pad2(i + 1)}`);

      // Month => Day*
      if (parsed.type === "Month") {
        const ret = [];
        // Needs to be local time else you end up in the previous day
        const date = new Date(int(parsed.year), int(parsed.mm) - 1);
        const month = date.getMonth();
        while (month === date.getMonth()) {
          const day = date.getDate();
          ret.push(`${parsed.month}-${pad2(day)}`);
          date.setDate(day + 1);
        }
        return ret;
      }
    }

    const NEXT = {
      Millennium: _ => {
        return `${pad4(_.millennium + 1)}/${pad4(_.millennium + 1000)}`;
      },
      Century: _ => (_.input === "-01" ? "01" : pad2(int(_.century) + 1)),
      Decade: _ => pad3(int(_.decade) + 1),
      Year: _ => pad4(int(_.year) + 1),
      Month: _ => {
        if (_.mm === "12") return next(_.year) + "-01";
        return _.bc + _.year + "-" + pad2(int(_.mm) + 1);
      },
      Day: _ => {
        const timestamp = Date.parse(_.day);
        if (isNaN(timestamp)) return;
        const date = new Date(timestamp);
        date.setDate(date.getDate() + 1);
        return _.bc + date.toISOString().slice(0, 10);
      },
    };

    const PREV = {
      Century: _ => (_.input === "01" ? "-01" : pad2(int(_.century) - 1)),
      Decade: _ => pad3(int(_.decade) - 1),
      Year: _ => pad4(int(_.year) - 1), // maybe a boundary case here
      Month: _ => {
        if (_.mm === "01") return previous(_.year) + "-12";
        return _.bc + _.year + "-" + pad2(int(_.mm) - 1);
      },
      Day: _ => {
        const timestamp = Date.parse(_.day);
        if (isNaN(timestamp)) return;
        const date = new Date(timestamp);
        date.setDate(date.getDate() - 1);
        return _.bc + date.toISOString().slice(0, 10);
      },
    };

    const subtract = (iso_a, iso_b) => {
      const a = parse(iso_a);
      const b = parse(iso_b);
      const type = a?.type;
      if (type !== b?.type) throw new Error(`Can't subtract ${iso_a}-${iso_b}`);
      switch (type) {
        case "Millennium":
          return { type, n: int(a.millennium) / 1e3 - int(b.millennium) / 1e3 };
        case "Century":
          // THIS will be off by one when spanning year zero, right?
          return { type, n: int(a.century) - int(b.century) };
        case "Decade":
          return { type, n: int(a.decade) - int(b.decade) };
        case "Year":
          return { type, n: int(a.year) - int(b.year) };
      }
      throw new Error(`Subtracting ${type} is not implemented`);
    };

    // analog of time:meets
    const next = date => {
      const parsed = parse(date);
      if (parsed) return NEXT[parsed.type](parsed);
    };
    // analog of time:isMetBy
    const previous = date => {
      const parsed = parse(date);
      if (parsed) return PREV[parsed.type](parsed);
    };

    const ORDINAL_SUFFIXES_EN = { 1: "st", 2: "nd", 3: "rd" };
    const ordinal_en = z => {
      const n = Math.abs(z);
      const c = n % 100;
      const suffix =
        c >= 11 && c <= 13 ? "th" : ORDINAL_SUFFIXES_EN[n % 10] ?? "th";
      return `${n}${suffix}`;
    };

    function label_en(iso) {
      const parsed = parse(iso);
      if (!parsed) {
        console.warn(`I don't recognize ‘${iso}’ as an ISO date`);
        return iso;
      }
      const { type, bc } = parsed;
      const maybe_bc = bc ? " BC" : ""; // yeah yeah BCE
      if (type === "Millennium") {
        const m = parsed.millennium / 1000;
        const mm = bc ? 1 - m : m;
        return `${ordinal_en(mm)} millennium${maybe_bc}`;
      }
      if (type === "Century") {
        const c = Math.abs(int(parsed.century)) + (bc ? -1 : 1);
        return `${ordinal_en(c)} century${maybe_bc}`;
      }
      if (type === "Decade") {
        // maybe add (decade) for first decade of a century
        return `${Math.abs(int(parsed.decade)) * 10}’s${maybe_bc}`;
      }
      if (type === "Year") {
        // TODO: wait BC years are off-by one, aren't they?
        return `${Math.abs(int(parsed.year))}${maybe_bc}`;
      }
      throw new Error(`Not implemented, labeling ${type}`);
    }

    // This isn't given by the ISO format per se, but is provided by ACDH
    function label(iso, lang = "en") {
      if (lang === "en") return label_en(iso);
      console.warn(`Language ‘${lang}’ is not supported for ISO labeling`);
      return iso; // fallback to ISO
    }

    return { parse, label, next, previous, subtract, broader, narrower };
  })();
  Object.assign(globalThis, { ISO8601 });
})();
