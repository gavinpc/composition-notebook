// For all titles read from stdin, emit a dictionary mapping them to time
// structures they identify (if any).

const readline = require("readline");
const { get_parser } = require("./parse-timeline-title");

function main(language) {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false,
  });

  const parser = get_parser(language);
  const annuals = {};
  const periods = {};
  const timeline = { annuals, periods };
  const dictionary = { language, timeline };

  rl.on("line", (title) => {
    const iso = parser.period_to_iso(title);
    if (iso) periods[title] = iso;
    else {
      const annual = parser.parse_annual(title);
      if (annual) annuals[title] = annual;
    }
  });

  rl.on("close", () => {
    process.stdout.write(JSON.stringify(dictionary));
  });
}

const [, , LANGUAGE = "en"] = process.argv;
main(LANGUAGE);
