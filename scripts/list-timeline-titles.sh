#!/bin/bash

# Emit the titles of all indexed timeline articles for a given language.
#
# ASSUMES jq is in path

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"
base_dir="${script_dir}/.."

language="${1:-en}"
cache_dir="${2:-${base_dir}/cache}"

index_file="${cache_dir}/wiki-data/timeline-index/${language}.json"

cat "$index_file" \
    | jq -r '.timeline | .periods,.annuals | keys | join("\n")' \

exit 0
