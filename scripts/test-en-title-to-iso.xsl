<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:func="http://exslt.org/functions"
               xmlns:notebook="https://gavinpc.com/composition-notebook"
               >
  <xsl:include href="./en-title-to-iso.xsl" />

  <xsl:template match="/">
    <xsl:for-each select="//test-cases//case">
      <xsl:variable name="got" select="notebook:en-title-to-iso(@given)" />
      <xsl:copy>
        <xsl:copy-of select="@* | node()" />
        <xsl:choose>
          <xsl:when test="$got=@expect">
            <pass />
            <xsl:if test="false()">
              <xsl:message>info: PASS <xsl:value-of select="concat(@given, ' → ', @expect)" /></xsl:message>
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <fail got="{$got}" />
            <xsl:message>
              <!-- redo with debug -->
              <xsl:variable name="got2" select="notebook:en-title-to-iso(@given, true())" />
              FAIL <xsl:value-of select="concat('for ‘', @given, '’ expected ‘', @expect, '’ but got ‘', $got2, '’')" />
            </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:copy>
    </xsl:for-each>
    <!-- for crashing build on failure -->
    <!-- <xsl:message terminate="yes">FAIL</xsl:message> -->
  </xsl:template>
</xsl:transform>
