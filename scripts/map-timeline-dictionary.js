// Write a timeline index for a language given an index as reference.
//
// There is *almost* nothing time-specific about this; most of this would apply
// as well to other types of metadata (e.g. persons, places, etc).  Just sayin'.

const fs = require("fs");

function die(message) {
  process.stderr.write(`abort:  ${message}\n`);
  process.exit(1);
}

const get_json_or_die = (file) => {
  try {
    return (
      JSON.parse(fs.readFileSync(file)) ||
      die(`expected non-empty JSON object at ${file}`)
    );
  } catch (error) {
    die(`expected valid JSON at ${file}`);
  }
};

// key_map is a map of titles in the target language to the reference language
// values is a map with keys in the reference language to some value
const map_links = (key_map, values) => {
  const dictionary = {};
  for (const key of Object.keys(key_map))
    if (values[key]) dictionary[key_map[key]] = values[key];
  return dictionary;
};

function main(links_file, reference_file, language, reference_language) {
  const { timeline } = get_json_or_die(reference_file);
  if (!timeline) die(`expected timeline structure at ${reference_file}`);
  const lang = get_json_or_die(links_file);
  const links = lang.links_to[reference_language];
  if (!links)
    die(`expected ${links_file} to have links for ${reference_language}`);
  const periods = map_links(links, timeline.periods);
  const annuals = map_links(links, timeline.annuals);
  const dictionary = { language, timeline: { periods, annuals } };
  process.stdout.write(JSON.stringify(dictionary));
}

const [, , LINKS_FILE, REFERENCE_FILE, LANGUAGE, REFERENCE_LANGUAGE] =
  process.argv;
main(LINKS_FILE, REFERENCE_FILE, LANGUAGE, REFERENCE_LANGUAGE);
