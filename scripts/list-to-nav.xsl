<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output method="html" omit-xml-declaration="yes" indent="yes" />
  <!-- Interpret the top-level items as links -->
  <xsl:template match="/*">
    <ul>
      <xsl:for-each select="*">
        <li>
          <a href="{.}"><xsl:value-of select="."/></a>
        </li>
      </xsl:for-each>
    </ul>
  </xsl:template>
</xsl:transform>
