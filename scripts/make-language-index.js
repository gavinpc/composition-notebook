// Regarding apostrophes:
// https://stackoverflow.com/a/18251730
// Wikipedia also doesn't encode colons in article references
/** Get title from the form in the langlinks reference to the form actually
 * expected in wiki links. */
const encode_title = (title) =>
  encodeURI(title.replace(/ /g, "_")).replace(/'/g, "%27");

/** Index a list of {title,langlinks} objects for lookup by language then
 * title. */
function make_cross_language_title_index(reference_lang, langlinks_iterable) {
  const languages = {};
  const index = { lang: reference_lang, languages };
  const errors = [];
  for (const { title, langlinks } of langlinks_iterable) {
    for (const langlink of langlinks) {
      const { "*": ref, langname, autonym, lang, url } = langlink;
      const wiki_ref = encode_title(ref);

      // Audit reference encoding
      const expected_url = `https://${lang}.wikipedia.org/wiki/${wiki_ref}`;
      if (url !== expected_url) errors.push({ url, expected_url });

      // `langname` and `autonym` are not canonical here, just for convenience.
      const { links } =
        languages[lang] || (languages[lang] = { langname, autonym, links: {} });

      links[title] = wiki_ref;
    }
  }
  // What was this for?
  // add_langlinks_to_index(index);
  return { index, errors };
}

module.exports = { make_cross_language_title_index };
