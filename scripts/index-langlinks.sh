#!/bin/bash

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"
base_dir="$script_dir/.."

# For the given language and list of articles, create an index of the articles
# in all other languages and print the name of the output directory.
#
# ASSUMES wikipedia tool relative to this script
# ASSUMES jq is in path.
# ASSUMES node is in path.
index_langlinks() {
    local reference_language="${1:-en}"
    local cache_dir="${2:-$base_dir/cache}"

    # This is an intermediate output and should only be relied on by this script.
    langlinks_dir="$cache_dir/wiki-data/langlinks-${reference_language}"
    xref_dir="$cache_dir/wiki-data/language-indexes-from/${reference_language}"

    index="$xref_dir/$language.json"

    mkdir -p "$langlinks_dir"
    mkdir -p "$xref_dir"

    # First extract the language links, then index them.
    while read title; do
	      echo "$title" >&2

	      # Ensure that we actually have this article.
	      parse_file=$("$base_dir/wikipedia/get-parse.sh" "$title" "$reference_language" "$cache_dir")
	      
	      # Write the language links to an intermediate file if it doesn't exist.
	      links_file="$langlinks_dir/${title}.json"
	      if [ ! -f "$links_file" ]; then
		        cat "$parse_file" | jq '.parse.langlinks' > "$links_file"
	      fi
    done

    node "$script_dir"/index-langlinks.js "$langlinks_dir" "$xref_dir" "$reference_language"

    echo "$xref_dir"
}

exit 0
