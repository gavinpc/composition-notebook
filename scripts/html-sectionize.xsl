<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:import href="./next.xsl" />
  
	<!-- Map old-style “heading-based” HTML documents into document outline. -->
  <!-- 
       Assumes that sectioning elements are not used.  That is, if a heading is
       used inside of an HTML5 sectioning element, it will create a new section
       anyway.
  -->

  <!-- Entry point -->
  <xsl:template match="/">
    <xsl:apply-templates mode="next" select="*" />
  </xsl:template>
  
  <!-- Headings become sections -->
  <xsl:template mode="next" match="h1 | h2 | h3 | h4 | h5 | h6 | h7">
    <xsl:param name="until" />
    
    <!-- Find the next same-level or higher-level (lower number) section, if any -->
    <xsl:variable name="level" select="number(substring-after(name(), 'h'))" />
    <xsl:variable
      name="break"
      select="(following-sibling::*
              [self::h1 or self::h2 or self::h3 or self::h4 or self::h5 or self::h6 or self::h7]
              [$level >= number(substring-after(name(), 'h'))]
              )[1]"
      />
    
    <!-- Emit the things between this node and the next section start. -->
    <section>
      <!-- The heading itself -->
      <xsl:copy>
        <xsl:copy-of select="@*" />
        <xsl:apply-templates mode="next" select="node()[1]" />
      </xsl:copy>
      
      <!-- The remaining content of this section, if any -->
      <xsl:variable name="next" select="(following-sibling::node())[1]" />
      <xsl:if test="$next and (not($break) or count($next | $break) > 1)">
        <xsl:apply-templates mode="next" select="$next">
          <xsl:with-param name="until" select="$break" />
        </xsl:apply-templates>
      </xsl:if>
    </section>
    
    <!-- The next section, if any -->
    <xsl:if test="$break and (not($until) or count($break | $until) > 1)">
      <xsl:apply-templates mode="next" select="$break">
        <xsl:with-param name="until" select="$until" />
      </xsl:apply-templates>
    </xsl:if>
  </xsl:template>
  
</xsl:transform>
