#!/bin/bash

# Optional second argument will be appended to API call.  You must include
# leading `&`.
# 
# ASSUMES curl and jq are in system path.

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"


# I suppose this is just given the JSON over stdin
wikipedia_sectionized_article() {
    cat \
        | jq -r '.parse.text."*"' \
        | xsltproc "$script_dir/html-sectionize.xsl" - \
                   > "$file"

}

wikipedia_get "$@"

exit 0
