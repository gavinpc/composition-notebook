#!/bin/bash

# Utility to fetch all of the articles in a cross-reference list.

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"
base_dir="$script_dir/.."

language="${1:-en}"
reference_language="${2:-en}"
cache_dir="${3:-$base_dir/cache}"

xref_dir="$cache_dir/language-indexes-from/${reference_language}"

index="$xref_dir/$language.json"

if [ ! -f "$index" ]; then
	echo "no cross reference file from '$reference_language' to '$language' in '$xref_dir'"
	exit 1
fi

cat "$index" | jq -r '.links_to.en[] | .' | while read title; do
	echo "$title" >&2
	"$base_dir"/wikipedia/get.sh "$title" "$language" "$cache_dir"
	sleep 1
done

exit 0
