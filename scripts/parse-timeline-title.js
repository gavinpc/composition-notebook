// Dispatcher for mapping Wikipedia article titles to time structures based on
// their naming conventions.  Per-language support must be provided.

const get_parser = (language) => {
  try {
    return require(`./languages/${language}/parse-titles`);
  } catch (error) {
    // Ignore, return undefined
  }
};

const parse_any = (language) => {
  const parser = get_parser(language);
  if (!parser) throw new Error(`could not find a parser for '${language}'`);
  return (title) => parser.parse_annual(title) || parser.parse_range(title);
};

module.exports = { get_parser, parse_any };
