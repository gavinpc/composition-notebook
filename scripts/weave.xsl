<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output mode="html" omit-xml-declaration="yes" />

  <xsl:template match="node()|@*" mode="weave">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" mode="weave" />
    </xsl:copy>
  </xsl:template>

  <!-- HTML5 script does not expect escapes -->
  <!-- this is also the case in style at least for > -->
  <xsl:template match="script|style" mode="weave">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="weave" />
      <xsl:value-of select="text()" disable-output-escaping="yes" />
    </xsl:copy>
  </xsl:template>

  <!-- prevent early-close on most elements -->
  <xsl:template match="*[not(node())][not(self::br or self::hr or self::link or self::img or self::meta)]" mode="weave">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="weave" />
      <xsl:comment />
    </xsl:copy>
  </xsl:template>

  <!-- naive, generic XML serialize -->
  <!-- unused now -->
  <xsl:template match="text()" mode="xml-serialize">
    <xsl:value-of select="." />
  </xsl:template>
  <xsl:template match="comment()" mode="xml-serialize">
    <xsl:text>&lt;!--</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>--&gt;</xsl:text>
  </xsl:template>
  <xsl:template match="@*" mode="xml-serialize">
    <xsl:value-of select="concat(' ', name(), '=&quot;', ., '&quot;')" />
  </xsl:template>
  <xsl:template match="*" mode="xml-serialize">
    <xsl:text>&lt;</xsl:text>
    <xsl:value-of select="name()" />
    <xsl:apply-templates select="@*" mode="xml-serialize" />
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates match="node()" mode="xml-serialize" />
    <xsl:text>&lt;/</xsl:text>
    <xsl:value-of select="name()" />
    <xsl:text>&gt;</xsl:text>
  </xsl:template>

  <!-- embed XML as DOM  -->
  <xsl:template match="node()|@*" mode="include">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" mode="include" />
    </xsl:copy>
  </xsl:template>

  <!-- An empty web link shows its address -->
  <xsl:template match="a[@href][not(node())]" mode="weave">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:value-of select="@href" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="*[@data-include-file]" mode="weave">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="weave" />
      <xsl:apply-templates select="document(@data-include-file)" mode="include" />
      <!-- Actually I'd expect such elements to be empty -->
      <!-- DUPLICATE early-close prevention.  I'm assuming you wouldn't use include on an early-close element -->
      <xsl:comment />
      <xsl:apply-templates select="node()" mode="weave" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
    <xsl:apply-templates select="." mode="weave" />
  </xsl:template>

</xsl:transform>
