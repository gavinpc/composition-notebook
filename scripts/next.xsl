<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<!-- An alternate identity copying mode. -->
  <!-- 
       Unlike the conventional identity template, this one does not descend into
       all child nodes at once.  Instead, it moves stepwise to the *first* child
       and the *first* following sibling.  This, plus a short-circuiting
       parameter, can be used to create traversals that are very awkward to
       express using the traditional descent.
  -->
  
  <!-- Entry point -->
  <xsl:template match="/">
    <xsl:apply-templates mode="next" select="*" />
  </xsl:template>

  <!-- Steps down and to the right. -->
  <xsl:template mode="next" match="node()">
    <!-- Short-circuit rightward traversal if this node is encountered. -->
    <xsl:param name="until" />
    
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates mode="next" select="node()[1]" />
    </xsl:copy>
    
    <xsl:variable name="next" select="(following-sibling::node())[1]" />
    
    <xsl:if test="$next and (not($until) or generate-id($next) != generate-id($until))">
      <xsl:apply-templates mode="next" select="$next">
        <xsl:with-param name="until" select="$until" />
      </xsl:apply-templates>
    </xsl:if>
  </xsl:template>

</xsl:transform>
