const fs = require("fs");
const path = require("path");
const { make_cross_language_title_index } = require("./make-language-index");

function* iterate_langlinks_directory(dir) {
  const files = fs.readdirSync(dir);
  for (const name of files) {
    const title = path.basename(name, ".json");
    const filename = path.join(dir, name);
    const text = fs.readFileSync(filename);
    let langlinks;
    try {
      langlinks = JSON.parse(text);
    } catch (error) {
      process.stderr.write(
        `warn: expected valid JSON for "${title}" at ${filename}\n`
      );
      continue;
    }

    if (!Array.isArray(langlinks)) {
      process.stderr.write(`warn: expected array for "${title}"\n`);
      continue;
    }

    yield { title, langlinks };
  }
}

/** Given a base language and a directory of language links (by title), create a
 * cross-reference file for each language. */
function main(dir, out_dir, reference) {
  const { index, errors } = make_cross_language_title_index(
    reference,
    iterate_langlinks_directory(dir)
  );

  for (const code of Object.keys(index.languages)) {
    const { links, ...rest } = index.languages[code];
    const language = { ...rest, links_to: { [reference]: links } };
    const out_file = path.join(out_dir, `${code}.json`);
    fs.writeFileSync(out_file, JSON.stringify(language));
  }

  for (const { url, expected_url } of errors)
    process.stderr.write(`expected\n  ${expected_url} instead of\n  ${url}\n`);
}

const [, , IN_DIR, OUT_DIR, reference_lang = "en"] = process.argv;
main(IN_DIR, OUT_DIR, reference_lang);
