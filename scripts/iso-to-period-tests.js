// Note that this could also be verified by cross-reference with LODE.

// Quotes are from the opening of the respective Wikipedia article.
const iso_to_period_tests = [
  // 0s The 0s, covers the first nine years of the Anno Domini era, which began
  // on January 1st, AD 1 and ended on December 31st, AD 9.
  ["000", { years: { first: 1, last: 9 } }],

  // 0s_BC This article concerns the period between 9 BC and 1 BC, the last nine
  // years of the Before Christ era.
  ["-000", { years: { first: -9, last: -1 } }],

  // 320s_BC This article concerns the period 329 BC – 320 BC.
  ["-032", { years: { first: -329, last: -320 } }],

  // 1660s The 1660s decade ran from January 1, 1660, to December 31, 1669.
  ["166", { years: { first: 1660, last: 1669 } }],

  // The 15th century was the century which spans the Julian years 1401 to 1500.
  ["15", { years: { first: 1401, last: 1500 } }],

  // The 15th century BC is a century which lasted from 1500 BC to 1401 BC.
  ["-15", { years: { first: -1500, last: -1401 } }],

  // 1st_millennium The first millennium was a period of time spanning the years
  // AD 1 to AD 1000
  //
  // See https://vocabs.acdh.oeaw.ac.at/date_entities/en/
  //
  ["0001/1000", { years: { first: 1, last: 1000 } }],
  // 1st_millennium_BC The 1st millennium BC is the period of time between from
  // the year 1000 BC to 1 BC
  ["-1000/-0001", { years: { first: -1000, last: -1 } }],

  // 2nd_millennium_BC The 2nd millennium BC spanned the years 2000 through 1001
  // BC.
  ["-2000/-1001", { years: { first: -2000, last: -1001 } }],

  // 2nd_millennium The second millennium was a period of time spanning the
  // years AD 1001 to 2000
  ["1001/2000", { years: { first: 1001, last: 2000 } }],

  // 1500s_(decade) The 1500s decade ran from January 1, 1500, to December 31,
  // 1509.
  ["150", { years: { first: 1500, last: 1509 } }],

  ["0005", { year: 5 }],
  ["0042", { year: 42 }],
  ["0399", { year: 399 }],
  ["1989", { year: 1989 }],

  // AD_1 AD 1 (I), 1 AD or 1 CE is the epoch year for the Anno Domini calendar
  // era.
  //
  // > While Year Zero does not exist in the Julian and Gregorian calendar,
  // > ISO8601 uses 0000 for 1 BC, and "-" (minus) for Before Christ (-0001 is 2
  // > BC).
  ["0001", { year: 1 }],
  ["0000", { year: -1 }],
  ["-0001", { year: -2 }],
  ["-0009", { year: -10 }],

  ["-1988", { year: -1989 }],
  ["-1480", { year: -1481 }],

  ["-0098", { year: -99 }],
];

module.exports = { iso_to_period_tests };
