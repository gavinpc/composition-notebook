<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex='http://exslt.org/common'
               >
  <xsl:include href="../bootstrap/sort-tuprules.xsl" />
  <xsl:include href="../bootstrap/normalize-tuprules.xsl" />

  <xsl:template match="/cases">
    <xsl:for-each select="case">
      <xsl:variable name="raw" select="ex:node-set(tuprule)" />
      <xsl:variable name="rules-data">
        <xsl:apply-templates select="$raw" mode="normalize-tuprule" />
      </xsl:variable>
      <xsl:variable name="rules" select="ex:node-set(ex:node-set($rules-data)/*)" />
      <xsl:message>info: ====== <xsl:value-of select="@name" /></xsl:message>
      <xsl:copy>
        <xsl:copy-of select="node()|@*" />
        <xsl:variable name="got-data">
          <xsl:call-template name="sort-tuprules">
            <xsl:with-param name="rules" select="$rules" />
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="got" select="ex:node-set(ex:node-set($got-data)/*)" />
        <got><xsl:copy-of select="$got" /></got>
        <!-- the correct sorting has ID's in order -->
        <xsl:variable name="early" select="$got[@id>following-sibling::*/@id]" />
        <xsl:variable name="pass" select="count($got)=count($rules) and not($early)" />

        <xsl:if test="not($pass)">
          <xsl:message terminate="yes">error: FAIL <xsl:value-of select="concat(count($rules), ' in ', count($got), ' out ', count($early), ' early: ')" /><xsl:for-each select="$got"><xsl:value-of select="concat(@id, ' ')" /></xsl:for-each></xsl:message>
        </xsl:if>
      </xsl:copy>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="/">
    <xsl:apply-templates />
  </xsl:template>
</xsl:transform>
