// Function for mapping ISO 8601 date representations into years or ranges of
// years.
//
// NOTE: This is not currently used.

const int = (s) => parseInt(s, 10);

/* === Periods ===  */

const FORMATS = [
  // Year
  //
  // > While Year Zero does not exist in the Julian and Gregorian calendar,
  // > ISO8601 uses 0000 for 1 BC, and "-" (minus) for Before Christ (-0001 is 2
  // > BC).
  [
    /^(-?)(\d{4})$/,
    (bc, digits, year = int(digits)) => ({
      year: int(bc || year === 0 ? -year - 1 : year),
    }),
  ],

  // Decade
  // Don't bother with exceptional cases for “0's” and “0's BC”
  [
    /^(-?)(\d{3})$/,
    (bc, s, n = int(s) * 10, first = bc ? -n - 9 : n) => ({
      years: { first, last: first + 9 },
    }),
  ],

  // Century
  [
    /^(-?)(\d{2})$/,
    (bc, s, n = int(s) * 100, first = bc ? n * -1 : n - 99) => ({
      years: { first, last: first + 99 },
    }),
  ],

  // Millennium
  [
    /^(-?)(\d{4})\/\1(\d{4})$/,
    (bc, d1, d2, first = int(d1), last = int(d2)) => {
      if (
        (bc && first % 1000 === 0 && first === last + 999) ||
        (!bc && last % 1000 === 0 && last === first + 999)
      ) {
        return { years: bc ? { first: -first, last: -last } : { first, last } };
      }
    },
  ],
];

/**
 * Return a structured representation of the given an ISO 8601 value.
 *
 * The latest version of the ISO 8601 as of this writing can be found at
 * https://www.iso.org/obp/ui#iso:std:iso:8601:-1:ed-1:v1:en
 *
 * Regarding the representation of millennia, see
 * https://vocabs.acdh.oeaw.ac.at/date_entities/en/
 */
const iso_to_period = (name) => {
  for (const [pattern, map] of FORMATS) {
    const match = name.match(pattern);
    if (match) return map(...match.slice(1));
  }
};

module.exports = { iso_to_period };
