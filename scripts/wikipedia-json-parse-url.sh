#!/bin/bash

set -e

# Emit the URL of a Wikimedia API request that returns a JSON payload including
# the markup for the given article.

# could be in XSLT at this point...
wikipedia_json_parse_url() {
    local title="$1"
    local language="${2:-en}"				# ISO 639
    local extra_args="$3"

    local api_base="https://$language.wikipedia.org/w/api.php"

    # Options to trim down unwanted output, mainly to omit transclusions.
    # 
    # I would like to omit *all* templates, particularly
    # `Template:Births_and_deaths_by_year_for_decade`, but the “template sandbox”
    # parameters are really just a testing mechanism.  So I just use it to mute the
    # template that accounts for the most transclusion.
    #
    # Strictly speaking, these are “application-specific” settings.  For the purpose
    # of creating the history dataset, in practice I want all caching of articles to
    # use the same settings.
    local streamline=''
    streamline+='&prop=text|revid|langlinks'
    streamline+="&templatesandboxtitle=Template:Events_by_year_for_decade"
    streamline+="&templatesandboxtext="
    streamline+="&disabletoc"
    streamline+="&disablelimitreport"
    streamline+="&disableeditsection"

    echo "${api_base}?action=parse&page=${title}&format=json${streamline}${extra_args}"
}

wikipedia_json_parse_url "$@"

exit 0
