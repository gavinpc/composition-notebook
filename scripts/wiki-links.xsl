<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" />

  <!-- Dump all (internal) wiki links from the given Mediawiki markup. -->
  <xsl:template match="/">
    <xsl:for-each select="//a[starts-with(@href, './')]">
      <xsl:value-of select="concat(substring-after(@href, './'), '&#xA;')" />
    </xsl:for-each>
  </xsl:template>

</xsl:transform>
