#!/bin/bash

# Make a reference language (if needed) and index all linked languages.

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"
base_dir="$script_dir/.."

reference_language="${1:-en}"
cache_dir="${2:-$base_dir/cache}"

wiki_data="$cache_dir/wiki-data"
xref_dir="$wiki_data/language-indexes-from/$reference_language"
reference_file="$wiki_data/timeline-index/$reference_language.json"

# This takes a little while so don't run if these exist already.
if [ ! -f "$reference_file" ]; then
    if [ ! -d "$xref_dir" ]; then
        "$script_dir"/make-reference-language.sh "$reference_language" "$cache_dir"
    fi
fi

for lang_path in "$xref_dir"/*.json; do
    lang_file="${lang_path##*/}"
    language="${lang_file%.json}"
    echo "$language" >&2
    "$script_dir"/index-timeline.sh "$language" "$reference_language" "$cache_dir" > /dev/null
done
                
exit 0
