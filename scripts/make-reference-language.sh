#!/bin/bash

# Create a language index from scratch, i.e. without a reference language.
# See README about requirements for a reference language.
#
# DELETES the contents of the list directory before proceeding.
#
# ASSUMES wiki-tools and wiki-language-index are in base dir.

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"
base_dir="$script_dir/.."

language="${1:-en}"
cache_dir="${2:-$base_dir/cache}"

save_outbound_links() {
    timeline_titles_dir="${cache_dir}/wiki-data/timeline-titles-$language"

    source_list_file="$script_dir/languages/$language/link-sources"
    all_titles_file="${timeline_titles_dir}/all"

    if [ ! -f "$source_list_file" ]; then
        echo "abort: no link source file at ${source_list_file}" >&2
        exit 1
    fi  

    mkdir -p "$timeline_titles_dir"
    rm -f "$timeline_titles_dir"/*

    cat "$source_list_file" | while read title; do
        echo "$title" >&2
        out_file="$timeline_titles_dir/$title"
        "$base_dir"/wiki-tools/wiki-links.sh "$title" "$language" "$cache_dir" > "$out_file"
    done

    cat "$timeline_titles_dir"/* | sort | uniq > "$all_titles_file"

    echo "$all_titles_file"
}

title_file=$(save_outbound_links)

dictionary_file="$cache_dir/wiki-data/timeline-index/$language.json"

mkdir -p "${dictionary_file%/*}"

# Create the reference list for the language itself.
cat "$title_file" \
    | node "$script_dir"/make-timeline-dictionary.js "$language" \
           > "$dictionary_file"

# Index all languages against this one for the titles identified as time values.
# This also prints the directory of the xref files.
"$script_dir"/list-timeline-titles.sh "$language" "$cache_dir" \
    | "$base_dir"/wiki-language-index/index-langlinks.sh "$language"

exit 0
