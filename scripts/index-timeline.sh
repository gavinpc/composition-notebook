#!/bin/bash

# Emit a dictionary of timeline article titles for a given language.

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"
base_dir="$script_dir/.."

language="$1"
reference_language="${2:-en}"
cache_dir="${3:-$base_dir/cache}"

wiki_data="$cache_dir/wiki-data"
timeline_index="$wiki_data/timeline-index"
dictionary_file="$timeline_index/$language.json"
reference_file="$timeline_index/$reference_language.json"
xref_dir="$wiki_data/language-indexes-from/${reference_language}"
links_file="$xref_dir/$language.json"

node "$script_dir"/map-timeline-dictionary.js "$links_file" "$reference_file" "$language" "$reference_language" \
	 > "$dictionary_file"

echo "$dictionary_file"

exit 0
