const { iso_to_period } = require("./iso-to-period");
const { get_parser } = require("./parse-timeline-title");
const { iso_to_period_tests } = require("./iso-to-period-tests");

const format_value = (value) =>
  value === undefined ? "undefined" : JSON.stringify(value);

const run_test = ({ fn, input, expect }) => {
  const got = fn(input);
  const pass = JSON.stringify(got) === JSON.stringify(expect);
  return { input, expect, pass, got };
};

const format_result = (_) =>
  _.pass
    ? `PASS ${_.input}`
    : `FAIL ${_.input} expected ${format_value(_.expect)} got ${format_value(
        _.got
      )}`;

const run_tests = (cases) => cases.map(run_test).map(format_result).join("\n");

function main(language) {
  const parser = get_parser("en");
  const cases = require(`./languages/${language}/parse-tests`);

  const annual_cases = cases.annual.map(([input, expect]) => ({
    fn: parser.parse_annual,
    input,
    expect,
  }));

  const period_to_iso_cases = cases.period.map(([input, expect]) => ({
    fn: parser.period_to_iso,
    input,
    expect,
  }));

  const iso_to_period_cases = iso_to_period_tests.map(([input, expect]) => ({
    fn: iso_to_period,
    input,
    expect,
  }));

  const annual_results = run_tests(annual_cases);
  const period_to_iso_results = run_tests(period_to_iso_cases);
  const iso_to_period_results = run_tests(iso_to_period_cases);

  console.log(`=== ANNUAL ===`);
  console.log(annual_results);
  console.log(`\n=== PERIOD TO ISO ===`);
  console.log(period_to_iso_results);
  console.log(`\n=== ISO TO PERIOD ===`);
  console.log(iso_to_period_results);
}

const [, , LANGUAGE = "en"] = process.argv;
main(LANGUAGE);
