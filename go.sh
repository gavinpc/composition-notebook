#!/bin/bash

# Run the system.

set -e

# `tup monitor` is not available on Windows (or MacOS):
if [ "$OS" == 'Windows_NT' ]; then
    >&2 echo "The system is not currently supported on ‘$OS’"
    exit 1
fi

# Complete a one-time build without parallelism before starting monitor.  This
# prevents the build from flooding remote hosts with requests.  It's kind of a
# hack, since nothing would stop such floods from happening if they were
# introduced while monitoring.  It also means that if the build is initially
# broken, then you need to keep re-running this script until you get it working
# again.
if tup "$@" -j1; then
    # Run in `foreground` mode as a background job so signals are propagated.
    trap 'tup stop' INT TERM
    tup monitor --autoupdate --foreground "$@" &
    monitor_pid="$!"
    wait "$monitor_pid"
fi

exit 0
