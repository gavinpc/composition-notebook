<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:param name="to-transform" select="'macroexpand.xsl'" />

  <!-- Include the input as a preamble to a transform -->
  <!-- Assumes I think that to-transform doesn't have an output element. -->
  <xsl:template match="/xsl:transform">
    <xsl:variable name="input" select="." />
    <xsl:for-each select="document($to-transform)/*">
      <xsl:copy>
        <xsl:copy-of select="@*" />
        <xsl:copy-of select="$input/*" />
        <xsl:copy-of select="node()" />
      </xsl:copy>
    </xsl:for-each>
  </xsl:template>
</xsl:transform>
