<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:str='http://exslt.org/strings'
               xmlns:func="http://exslt.org/functions"
               exclude-result-prefixes='str'
               extension-element-prefixes="func"
               >
  <xsl:output mode="text" omit-xml-declaration="yes" indent="no" />

  <xsl:variable name="CR" select="'&#xA;'" />

  <func:function name="str:list">
    <xsl:param name="tokens" />
    <func:result>
      <xsl:for-each select="$tokens">
        <xsl:value-of select="concat(., ' ')" />
      </xsl:for-each>
    </func:result>
  </func:function>

  <!-- Pass-through -->
  <xsl:template match="node()" mode="tup1">
    <xsl:apply-templates mode="tup1" />
  </xsl:template>

  <!-- one-line Tup rule -->
  <!-- should be the only place structured Tup rules are serialized -->
  <xsl:template match="tuprule" mode="tup1">
    <xsl:variable name="group">
      <xsl:choose>
        <xsl:when test="@group"><xsl:value-of select="@group" /></xsl:when>
        <xsl:otherwise>default</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:text>: </xsl:text>
    <xsl:if test="foreach">foreach </xsl:if>
    <xsl:value-of
      disable-output-escaping="yes"
      select="concat(
              str:list(input), ' | ', str:list(extra-input),
              '|> ^',  @flags, ' ', @label, '^ ', @process,
              '|> ', str:list(output), ' | ', str:list(extra-output), '&lt;', $group, '>',
              $CR)" />
  </xsl:template>

  <xsl:template match="/" mode="serialize-tuprules">
    <xsl:apply-templates select="." mode="tup1" />
  </xsl:template>

  <!-- Default -->

  <xsl:template match="/">
    <xsl:apply-templates select="." mode="serialize-tuprules" />
  </xsl:template>

</xsl:transform>
