<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex='http://exslt.org/common'
               xmlns:str='http://exslt.org/strings'
               exclude-result-prefixes='str'
               >
  <xsl:output mode="text" omit-xml-declaration="yes" indent="no" />
  <xsl:param name="ROOT" select="'..'" />

  <xsl:variable name="CR" select="'&#xA;'" />


	<xsl:template match="text()" mode="serialize-shell" />
	<xsl:template match="tuprule" mode="serialize-shell">
    <xsl:text># </xsl:text>
    <xsl:value-of disable-output-escaping="yes" select="concat(@label, $CR)" />

    <xsl:variable name="inputs">
      <xsl:for-each select="input">
        <xsl:if test="position() > 1"> </xsl:if>
        <xsl:value-of select="." />
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="outputs">
      <xsl:for-each select="output">
        <xsl:if test="position() > 1"> </xsl:if>
        <xsl:value-of select="." />
      </xsl:for-each>
    </xsl:variable>

    <!-- Substitutions -->
    <xsl:variable
        name="process"
        select="str:replace(
                str:replace(
                str:replace(
                @process,
                  '%f', $inputs),
                  '%o', $outputs),
                  '$(ROOT)', $ROOT)
                  "
        />

    <!-- Ensure the output directories exist -->
    <xsl:variable name="out-dirs-data">
      <xsl:for-each select="output | extra-output">
        <xsl:variable name="expanded" select="str:replace(., '$(ROOT)', $ROOT)" />
        <xsl:variable name="parts" select="str:split($expanded, '/')" />
        <dir>
          <xsl:for-each select="$parts[last() > position()]">
            <xsl:value-of select="concat(., '/')" />
          </xsl:for-each>
        </dir>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="out-dirs" select="ex:node-set($out-dirs-data)" />
    <xsl:for-each select="$out-dirs/dir[not(.=preceding-sibling::dir)]">
      <xsl:value-of select='concat("mkdir -p &apos;", ., "&apos;", $CR)' />
    </xsl:for-each>

    <xsl:if test="foreach"># NOT TODAY </xsl:if>
    <xsl:value-of disable-output-escaping="yes" select="concat($process, $CR)" />
  </xsl:template>

  <xsl:template match="/">
    <xsl:apply-templates mode="serialize-shell" />
  </xsl:template>

</xsl:transform>
