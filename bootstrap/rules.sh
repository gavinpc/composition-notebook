#!/bin/bash

# Emit the tup rules from the HTML files in CWD.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

# Assume a non-cygwin xsltproc even if we're in cygwin
if [ "$OSTYPE" = 'cygwin' ]; then
   this_script_dir=$(cygpath -m $this_script_dir)
fi

bind_notebook() {
    echo '<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1" />
    <link rel="icon" href="data:," />
    <title>composition notebook</title>
  </head>
  <body>'
    # bootstrapper goeth here
    # cover / front matter goeth here
    cat *.html
    echo '</body></html>'
}

# Create a transform from HTML documents
extract_xsl() {
    echo '<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ex="http://exslt.org/common" xmlns:str="http://exslt.org/strings" xmlns:date="http://exslt.org/dates-and-times" exclude-result-prefixes="ex date">'
    xsltproc --html "$this_script_dir"/extract-xsl-and-macros.xsl "$@" 2>/dev/null
    echo '</xsl:transform>'
}

get_userland_rules() {
    # extract_xsl *.html
    extract_xsl <(bind_notebook)
}

get_macroexpand_transform() {
    # xsltproc "$this_script_dir"/include.xsl <(get_userland_rules)
    # above fails on Windows with something like
    #
    #     warning: failed to load external entity "/proc/61438/fd/63"
    #     unable to parse /proc/61438/fd/63
    get_userland_rules | xsltproc "$this_script_dir"/include.xsl -
}

xslt_html() {
    xsltproc --encoding utf-8 --html "$@" \
             2> >(sed '/\(Tag.*invalid\|Unexpected end tag\)/,+2d' >&2)
}

get_directives() {
    echo '<rules>'
    # Avoid creating a temp file.  Also, sending the transform via stdin
    # (versus, say, a process substitution) prevents xsltproc from resolving
    # paths against `/dev/fd/n`, which would break xsl:include and document().
    local expand=$(get_macroexpand_transform)
    for file in *.html; do
        # PENDING new pipeline
        # echo "$expand" | xslt_html - <(bind_notebook) 
        echo "$expand" | xslt_html - "$file" \
            | xslt_html \
                  --stringparam single-source "$file" \
                  "$this_script_dir"/rules.xsl -
    done
    echo '</rules>'
}

# EXPERIMENTAL: Parallel version of above
get_directives_para() {
    echo '<rules>'
    # Avoid creating a temp file.  Also, sending the transform via stdin
    # (versus, say, a process substitution) prevents xsltproc from resolving
    # paths against `/dev/fd/n`, which would break xsl:include and document().
    local expand=$(get_macroexpand_transform)
    echo "$expand" > "$this_script_dir"/macros.xsl
    echo *.html \
        | xargs -n1 -P0 bash -c 'for file; do printf "%s\n" "$(cat '"$this_script_dir"'/macros.xsl | xsltproc --html - "$file" 2>/dev/null | xsltproc --html --stringparam single-source "$file" '"$this_script_dir"'/rules.xsl - 2>/dev/null)"; done' _
    echo '</rules>'
    rm "$this_script_dir"/macros.xsl
}

normalized() {
    get_directives \
        | xsltproc "$this_script_dir"/normalize-tuprules.xsl -
}

gitignore() {
    # coz this doesn't need to be sorted, right?
    normalized \
        | xsltproc "$this_script_dir"/serialize-gitignore.xsl -
}

sorted() {
    normalized \
        | xsltproc "$this_script_dir"/sort-tuprules.xsl -
}

generate() {
    normalized \
        | xsltproc "$this_script_dir"/serialize-shell.xsl -
}

serialized() {
    sorted \
        | xsltproc "$this_script_dir"/serialize-tuprules.xsl -
}

serialized_in_process() {
    get_directives \
        | xsltproc "$this_script_dir"/process-tuprules.xsl -
}

expand_one() {
    local file="$1"
    local xform="$2"
    if [ -z "$xform" ]; then
        xslt_html <(get_macroexpand_transform) "$file"
    elif [ -f "$xform" ]; then
         xslt_html "$xform" "$file"
    else
        >&2 echo "No such file ‘$xform’"
        return 1
    fi
}

full() {
    # Tup is fussy about run script output: even blank lines will throw
    serialized_in_process \
        | sed '/^ *$/d'
}

if [[ "$1" =~ ^bind ]]; then bind_notebook
elif [[ "$1" =~ ^raw ]]; then get_directives
elif [[ "$1" =~ ^norm ]]; then normalized
elif [[ "$1" =~ ^sort ]]; then sorted
elif [[ "$1" =~ ^ser ]]; then serialized
elif [[ "$1" =~ ^gen ]]; then generate
elif [[ "$1" =~ ^git ]]; then gitignore
elif [[ "$1" =~ ^xsl ]]; then get_userland_rules
elif [[ "$1" =~ ^mac ]]; then get_macroexpand_transform
elif [[ "$1" =~ ^exp ]]; then expand_one "$2" "$3"
else full
fi

exit 0
