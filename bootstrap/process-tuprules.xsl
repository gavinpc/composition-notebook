<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex='http://exslt.org/common'
               exclude-result-prefixes='ex'
               >
  <xsl:include href="./normalize-tuprules.xsl" />
  <xsl:include href="./sort-tuprules.xsl" />
  <xsl:include href="./serialize-tuprules.xsl" />

  <!-- Pipeline that does the same thing as the serialize step in rules.sh -->
  <!-- But without all of the intermediate process & I/O -->
  
  <!-- I don't care about verbosity here *per se* -->
  <!-- Rather, I want something isomorphic with the form in rules.sh -->
  
  <xsl:template match="/">
    <xsl:variable name="normalized-data">
      <xsl:apply-templates select="." mode="normalize-tuprules" />
    </xsl:variable>
    <xsl:variable name="normalized" select="ex:node-set($normalized-data)" />
    
    <xsl:variable name="sorted-data">
      <xsl:apply-templates select="$normalized" mode="sort-tuprules" />
    </xsl:variable>
    <xsl:variable name="sorted" select="ex:node-set($sorted-data)" />
    
    <xsl:apply-templates select="$sorted" mode="tup1" />
  </xsl:template>
  
</xsl:transform>
