<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:key name="tangle" match="*[@data-tangle-file]" use="@data-tangle-file" />
  
  <!-- Tangles -->
  <xsl:template match="*" mode="tangle-rules">
    <xsl:param name="source" select="$single-source" />
    <!--
    <xsl:message>debug tangle file = <xsl:value-of select="$source" /></xsl:message>
    -->
    <!-- One file may use many blocks -->
    <xsl:variable
      name="tangles"
      select="//*[@data-tangle-file][generate-id() = generate-id(key('tangle', @data-tangle-file)[1])]" />

    <xsl:if test="$tangles">
      <xsl:for-each select="$tangles">
        <xsl:variable name="target" select="@data-tangle-file" />
        <notebook-tuprule
          input="~/expanded/{$source}"
          extra-input="~/expanded/*.html"
          process="$(ROOT)/bootstrap/tangle.sh '%f' '%o'"
          output="{$target}"
          flags="o"
          label="tangle {$target} from {$source}"
          />
      </xsl:for-each>
    </xsl:if>

  </xsl:template>

</xsl:transform>
