<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output omit-xml-declaration="yes" />

  <xsl:template match="node()|@*" mode="insert-before-begin" />
  <xsl:template match="node()|@*" mode="insert-attributes" />
  <xsl:template match="node()|@*" mode="insert-after-begin" />
  <xsl:template match="node()|@*" mode="insert-before-end" />
  <xsl:template match="node()|@*" mode="insert-after-end" />

  <xsl:template match="node()|@*" mode="macroexpand">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" mode="macroexpand" />
    </xsl:copy>
  </xsl:template>

  <!-- Leaving these in is kind of hazardous but how else do we retain them?  -->
  <!-- 
  <xsl:template match="*[starts-with(name(), 'xsl-')]" mode="macroexpand"/>
  -->
  <!-- Containers are disposable.  Still, this might not be right place to do this.  -->
  <xsl:template match="html | body | main" mode="macroexpand">
    <xsl:apply-templates select="node()" mode="macroexpand" />
  </xsl:template>
  <!-- STOPGAP: preventing breakage but see notes about weave versus macroexpand -->
  <xsl:template match="*[self::style or self::script]/text()" mode="macroexpand">
    <xsl:value-of select="." disable-output-escaping="yes" />
  </xsl:template>
  <xsl:template match="*" mode="macroexpand">
    <xsl:apply-templates select="." mode="insert-before-begin" />
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="macroexpand" />
      <xsl:apply-templates select="." mode="insert-attributes" />
      <xsl:apply-templates select="." mode="insert-after-begin" />
      <xsl:apply-templates select="node()" mode="macroexpand" />
      <xsl:apply-templates select="." mode="insert-before-end" />
    </xsl:copy>
    <xsl:apply-templates select="." mode="insert-after-end" />
  </xsl:template>

  <!-- Default -->

  <xsl:template match="/">
    <xsl:apply-templates select="." mode="macroexpand" />
  </xsl:template>

</xsl:transform>
