<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex='http://exslt.org/common'
               exclude-result-prefixes='ex'
               >
  <xsl:output indent="yes" />

  <xsl:key name="rule" match="tuprule" use="@id" />
  <xsl:key use="." name="in" match="input | extra-input" />
  <xsl:key use="." name="out" match="@group | output | extra-output" />
  <xsl:key use="." name="in-dir" match="input-dir" />
  <xsl:key use="." name="out-dir" match="output-dir" />

  <xsl:template match="node()|@*" mode="sort-rules-emit">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" mode="sort-rules-emit" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="*[ancestor::group]" mode="sort-rules-emit">
    <xsl:copy-of select="ancestor::group/*" />
  </xsl:template>

  <!-- Copy rules discarding dead edges -->
  <!-- couldn't we discard other things too? -->
  <xsl:template match="@* | node()" mode="reduce">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="reduce" />
    </xsl:copy>
  </xsl:template>

  <!-- groups are represented by their first rule -->
  <xsl:template match="group" mode="reduce">
    <xsl:apply-templates select="*[1]" mode="reduce" />
  </xsl:template>

  <xsl:template match="output[not(key('in', .))]" mode="reduce" />
  <xsl:template match="extra-output[not(key('in', .))]" mode="reduce" />
  <xsl:template match="output-dir[not(key('in-dir', .))]" mode="reduce" />

  <xsl:template match="input[not(key('out', .))]" mode="reduce" />
  <xsl:template match="extra-input[not(key('out', .))]" mode="reduce" />
  <xsl:template match="input-dir[not(key('out-dir', .))]" mode="reduce" />

  <!--  Assumes “reduced” rules, meaning all inputs and outputs have a referent -->
  <!-- at least on initial call... but is that an invariant? -->
  <xsl:template name="sort-tuprules-inner">
    <xsl:param name="all-rules" />  <!--a node set-->
    <xsl:param name="to-sort" />  <!--a node set-->
    <xsl:param name="depth">0</xsl:param>
    <xsl:param name="debug" />

    <xsl:if test="$debug">
      <xsl:message>debug: round <xsl:value-of select="$depth+1"/></xsl:message>
      <xsl:message>debug: <xsl:value-of select="count($to-sort)"/> rules to sort</xsl:message>
    </xsl:if>

    <!-- Base case: which rules need no input now? -->
    <xsl:variable name="ready" select="$to-sort[not(input-dir|input|extra-input)]" />

    <!-- We won't terminate if this happens, unless the number of rules given was zero -->
    <!-- This probably indicates a circularity.  A good thing to do here would be find and report it. -->
    <xsl:if test="count($ready) = 0">
      <xsl:if test="$debug">
        <xsl:message>
          <xsl:for-each select="$to-sort[input | input-dir | extra-input]">
            <xsl:value-of select="concat($CR, @id)" /> for <xsl:value-of select="output | extra-output | output-dir" />
            <xsl:text> still needs:</xsl:text>
            <xsl:for-each select="input"><xsl:value-of select="concat($CR, ' input ', .)" /></xsl:for-each>
            <xsl:for-each select="input-dir"><xsl:value-of select="concat($CR, ' input dir ', .)" /></xsl:for-each>
            <xsl:for-each select="extra-input"><xsl:value-of select="concat($CR, ' extra ', .)" /></xsl:for-each>
        </xsl:for-each></xsl:message>
      </xsl:if>
      <xsl:message terminate="yes">No rules ready in this round!</xsl:message>
    </xsl:if>

    <xsl:if test="$debug">
      <xsl:message>verbose: ready outs: <xsl:for-each select="$all-rules[@id = $ready/@id]/*[contains(name(), 'output')]"><xsl:value-of select="concat($CR, ' - ', .)" /></xsl:for-each></xsl:message>
    </xsl:if>

    <!-- Emit those. Can't just emit $ready directly because it's reduced -->
    <!-- Equivalent to the slower form -->
    <!-- <xsl:copy-of select="$all-rules[@id=$ready/@id]" /> -->
    <xsl:for-each select="$ready">
      <xsl:variable name="id" select="@id" />
      <xsl:for-each select="$all-rules[1]">
        <xsl:apply-templates select="key('rule', $id)" mode="sort-rules-emit"/>
      </xsl:for-each>
    </xsl:for-each>

    <xsl:variable name="others" select="$to-sort[not(@id=$ready/@id)]" />

    <!-- Construct remaining rules minus inputs just covered -->
    <xsl:variable name="rest-data">
      <xsl:for-each select="$others">
        <xsl:copy>
          <xsl:copy-of select="@id | @group | output-dir | output | extra-output" />
          <xsl:copy-of select="(input | extra-input)[not(. = $ready/output) and not(. = $ready/extra-output)]" />
          <xsl:copy-of select="input-dir[not(. = $ready/output-dir)]" />
        </xsl:copy>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="rest" select="ex:node-set(ex:node-set($rest-data)/*)" />

    <xsl:if test="$debug">
      <xsl:message>debug: <xsl:value-of select="count($ready)"/> ready</xsl:message>
      <xsl:message>debug: <xsl:value-of select="count($others)"/> others</xsl:message>
      <xsl:message>debug: <xsl:value-of select="count($rest)"/> rest</xsl:message>
      <xsl:message>debug: <xsl:value-of select="count($rest/*)"/> i/o</xsl:message>
      <xsl:if test="count($rest) = 0"><xsl:message>debug: DONE!</xsl:message></xsl:if>
    </xsl:if>

    <xsl:if test="count($rest) != 0 and $depth &lt; 20">
      <xsl:call-template name="sort-tuprules-inner">
        <xsl:with-param name="all-rules" select="$all-rules" />
        <xsl:with-param name="to-sort" select="$rest" />
        <xsl:with-param name="depth" select="$depth + 1" />
        <xsl:with-param name="debug" select="$debug" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- Invariants: -->
  <!-- Always emit all of the rules in the given set exactly once each -->
  <!-- such that all rules precede their dependencies -->
  <!-- ASSUMES rules in normal form -->
  <xsl:template name="sort-tuprules">
    <xsl:param name="rules" />  <!--a node set-->
    <xsl:param name="debug" select="false()" />

    <xsl:variable name="to-sort-data">
      <xsl:apply-templates select="$rules" mode="reduce" />
    </xsl:variable>
    <xsl:variable name="to-sort" select="ex:node-set(ex:node-set($to-sort-data)/*)" />

    <xsl:if test="$debug">
      <xsl:message>debug: <xsl:value-of select="count($rules/*)"/> i/o</xsl:message>
      <xsl:message>debug: <xsl:value-of select="count($to-sort/*)"/> i/o to sort</xsl:message>
    </xsl:if>

    <!-- recur on whatever's left -->
    <xsl:if test="count($to-sort) != 0">
      <xsl:call-template name="sort-tuprules-inner">
        <xsl:with-param name="all-rules" select="$rules" />
        <xsl:with-param name="to-sort" select="$to-sort" />
        <xsl:with-param name="debug" select="$debug" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- ASSUMES incoming rules are normalized -->
  <xsl:template match="/" mode="sort-tuprules">
    <rules>
      <xsl:call-template name="sort-tuprules">
        <xsl:with-param name="rules" select="ex:node-set(/rules/*)" />
      </xsl:call-template>
    </rules>
  </xsl:template>

  <!-- Default -->

  <xsl:template match="/">
    <xsl:apply-templates select="." mode="sort-tuprules" />
  </xsl:template>
  
</xsl:transform>
