#!/bin/bash

# Compose and write TARGET from designated blocks in SOURCE.
#
# For the most part, this is just about emitting text, so we could just write
# result to `stdout` without getting involved in actually writing target.
#
# However, some files need to be made executable, so we do need to know and
# write to the actual location.

set -e

# Both paths are relative to the originating directory
source="$1"
target="$2"

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
# Assume a non-cygwin xsltproc even if we're in cygwin
if [ "$OSTYPE" = 'cygwin' ]; then
   this_script_dir=$(cygpath -m $this_script_dir)
fi

bootstrap="$this_script_dir"
project="${bootstrap%/*}"

# canonicalize path
composite="$PWD/$target"
file=$(realpath "$composite")
if [ "$OSTYPE" = 'cygwin' ]; then
   file=$(cygpath -m $file)
fi
if [[ "$file" != "$project"* ]]; then
    >&2 echo "Expected $file to begin with $project (for $composite)"
    exit 1
fi
# won't this be bad on regex special characters though?
canonical="${file/$project/\~}"

xsltproc --html --novalid --stringparam target "$canonical" \
         "$this_script_dir"/tangle.xsl "$source" \
         2> >(sed '/Tag.*invalid/,+2d' >&2) \
         > "$target"

# heuristic: make .sh files executable
if [[ "$target" =~ .sh$ ]]; then chmod +x "$target"; fi

exit 0
