<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               exclude-result-prefixes='xsl'>
  <xsl:output omit-xml-declaration="yes" />

  <xsl:template match="node()|@*" mode="xsl-ns">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" mode="xsl-ns" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="*[starts-with(name(), 'xsl-')]" mode="xsl-ns">
    <xsl:element name="xsl:{substring-after(name(), 'xsl-')}">
      <xsl:apply-templates select="node()|@*" mode="xsl-ns" />
    </xsl:element>
  </xsl:template>

  <xsl:template match="node()|@*" mode="extract-xsl-and-macros">
    <xsl:apply-templates select="node()|@*" mode="extract-xsl-and-macros" />
  </xsl:template>
  <!-- this seems to be necessary but why? -->
  <xsl:template match="text()" mode="extract-xsl" />
  <xsl:template  match="*[starts-with(name(), 'xsl-')]" mode="extract-xsl-and-macros">
    <xsl:apply-templates select="." mode="xsl-ns" />
  </xsl:template>

  <!-- Can't include xsl:transform wrapper here if processing multiple files -->
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="extract-xsl-and-macros" />
  </xsl:template>

</xsl:transform>
