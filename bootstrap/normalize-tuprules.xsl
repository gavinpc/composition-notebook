<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:str='http://exslt.org/strings'
               xmlns:func="http://exslt.org/functions"
               xmlns:notebook="https://gavinpc.com/composition-notebook"
               exclude-result-prefixes='str notebook'
               extension-element-prefixes="func"
               >
  <xsl:output indent="yes" />

  <func:function name="str:join">
    <xsl:param name="tokens" />
    <xsl:param name="delim" select="''" />
    <func:result>
      <xsl:for-each select="$tokens">
        <xsl:if test="position() > 1"><xsl:value-of select="$delim" /></xsl:if>
        <xsl:value-of select="." />
      </xsl:for-each>
    </func:result>
  </func:function>

  <func:function name="notebook:dirname">
    <xsl:param name="path" select="." />
    <xsl:variable name="parts" select="str:split($path, '/')" />
    <xsl:variable name="all-but-last" select="$parts[not(position() = last())]" />
    <func:result select="concat(str:join($all-but-last, '/'), '/')" />
  </func:function>

  <func:function name="notebook:expand-project-path">
    <xsl:param name="text" select="." />
    <func:result>
      <xsl:choose>
        <xsl:when test="starts-with($text, '~/')">
          <xsl:value-of select="concat('$(ROOT)/', substring-after($text, '~/'))" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$text" />
        </xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>

  <xsl:template match="node()|@*" mode="input-path">
    <xsl:param name="name" select="name()" />
    <xsl:variable name="path" select="notebook:expand-project-path(.)" />

    <!-- HACK: naive test for wildcard -->
    <xsl:if test="contains($path, '*')">
      <input-dir>
        <xsl:value-of select="notebook:dirname($path)" />
      </input-dir>
    </xsl:if>

    <xsl:element name="{$name}">
      <xsl:value-of select="$path" />
    </xsl:element>
  </xsl:template>

  <xsl:template match="node()|@*" mode="output-path">
    <xsl:param name="name" select="name()" />
    <xsl:variable name="path" select="notebook:expand-project-path(.)" />
    <xsl:element name="{$name}">
      <xsl:value-of select="$path" />
    </xsl:element>
    <xsl:if test="contains($path, '/')">
      <output-dir>
        <xsl:value-of select="notebook:dirname($path)" />
      </output-dir>
    </xsl:if>
  </xsl:template>

  <!-- mostly pass through -->
  <xsl:template match="*" mode="normalize-tuprule">
    <xsl:apply-templates select="*" mode="normalize-tuprule"/>
  </xsl:template>

  <!-- Support shorthand notation for tup rules -->
  <xsl:template match="notebook-tuprule" mode="normalize-tuprule">
    <tuprule id="{generate-id()}">
      <xsl:copy-of select="@id | @flags | @label | @process | @group" />

      <xsl:apply-templates select="str:split(@inputs)" mode="input-path">
        <xsl:with-param name="name" select="'input'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="str:split(@extra-inputs)" mode="input-path">
        <xsl:with-param name="name" select="'extra-input'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="str:split(@outputs)" mode="output-path">
        <xsl:with-param name="name" select="'output'" />
      </xsl:apply-templates>
      <xsl:apply-templates select="str:split(@extra-outputs)" mode="output-path">
        <xsl:with-param name="name" select="'extra-output'" />
      </xsl:apply-templates>

      <xsl:apply-templates select="@input | @extra-input" mode="input-path" />
      <xsl:apply-templates select="@output | @extra-output" mode="output-path" />

      <xsl:copy-of select="node()" />
    </tuprule>
  </xsl:template>

  <xsl:template match="group" mode="normalize-tuprule">
    <xsl:copy>
      <xsl:apply-templates mode="normalize-tuprule" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/" mode="normalize-tuprules">
    <rules>
      <xsl:apply-templates select="." mode="normalize-tuprule" />
    </rules>
  </xsl:template>

  <!-- Default -->

  <xsl:template match="/">
    <xsl:apply-templates select="." mode="normalize-tuprules" />
  </xsl:template>

</xsl:transform>
