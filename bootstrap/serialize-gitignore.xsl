<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output mode="text" omit-xml-declaration="yes" indent="no" />

  <xsl:variable name="CR" select="'&#xA;'" />

  <xsl:template match="node() | @*" mode="serialize-gitignore">
    <xsl:value-of disable-output-escaping="yes" select="concat('# what have we here?', name(), ' = ', .)" />
  </xsl:template>

  <xsl:template match="tuprule" mode="serialize-gitignore">
    <!-- foreach rules aren't supported here because they can have % and such -->
    <!-- Is that not true of normal rules, though? -->
    <xsl:if test="foreach">
      <xsl:value-of disable-output-escaping="yes" select="concat('# not implemented: foreach ', ., $CR)" />
    </xsl:if>
    <xsl:if test="not(foreach)">
      <xsl:for-each select="output | extra-output">
        <xsl:choose>
          <xsl:when test="starts-with(., '$(ROOT)/')">
            <xsl:variable name="ignore" select="substring-after(., '$(ROOT)/')" />
            <xsl:value-of disable-output-escaping="yes" select="concat($ignore, $CR)" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of disable-output-escaping="yes" select="concat('# WARN: non-rooted path: ', ., $CR)" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <xsl:template match="/">
    <xsl:value-of disable-output-escaping="yes" select="concat('# THIS IS A GENERATED FILE!', $CR)" />
    <xsl:value-of disable-output-escaping="yes" select="concat('\.gitignore', $CR)" />
    <xsl:value-of disable-output-escaping="yes" select="concat('\.tup', $CR)" />
    <xsl:apply-templates select="rules/*" mode="serialize-gitignore" />
  </xsl:template>

</xsl:transform>
