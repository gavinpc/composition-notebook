<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output method="xml" omit-xml-declaration="yes" indent="yes" />
  <xsl:include href="tangle-expand.xsl" />
  <xsl:param name="single-source" />

  <xsl:template match="*" mode="doc">
    <xsl:param name="file" select="$single-source" />

    <rules from="{$file}">
      <xsl:apply-templates select="." mode="tangle-rules">
        <xsl:with-param name="source" select="$file" />
      </xsl:apply-templates>
      <xsl:copy-of select="//notebook-tuprule[not(ancestor::*[starts-with(name(), 'xsl-')])]" />
    </rules>

  </xsl:template>

  <xsl:template match="/">
    <xsl:apply-templates select="." mode="doc" />
  </xsl:template>
	
</xsl:transform>
