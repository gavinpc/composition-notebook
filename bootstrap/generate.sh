#!/bin/bash

# Initial attempt at mimicing `tup generate`.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

"$this_script_dir"/rules.sh generate

exit 0
